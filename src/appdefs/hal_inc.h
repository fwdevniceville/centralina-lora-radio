/*
 * hal_inc.h
 *
 *  Created on: 11 mar 2018
 *      Author: daniele_parise
 */

#ifndef SRC_APPLICATION_HWBRD_HAL_INC_H_
#define SRC_APPLICATION_HWBRD_HAL_INC_H_

#include <stdbool.h>
#include <stdint.h>

#include "stm32l0xx_hal.h"
#include "stm32l0xx_hal_uart_ex.h"

#endif /* SRC_APPLICATION_HWBRD_HAL_INC_H_ */
