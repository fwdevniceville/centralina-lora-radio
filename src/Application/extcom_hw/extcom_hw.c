/*
 * extcom_hw.c
 *
 *  Created on: 18 dic 2018
 *      Author: daniele_parise
 */
#include <stdbool.h>
#include <stdint.h>
#include "../extcom/extcom.h"
#include "extcom_hw.h"
#include <string.h>
#include "../hwbrd/hw_bridge.h"
#include "low_power_manager.h"


void extcom_hwEnterStopMode(void)
{
	HAL_UARTEx_EnableStopMode(&UartExtcomHandle);
//	SET_BIT(UartIntcomHandle.Instance->CR1, USART_CR1_UESM);

}

void extcom_hwExitStopMode(void)
{
	HAL_UARTEx_DisableStopMode(&UartExtcomHandle);
//	CLEAR_BIT(UartIntcomHandle.Instance->CR1, USART_CR1_UESM);
}
