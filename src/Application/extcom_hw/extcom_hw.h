/*
 * extcom_hw.h
 *
 *  Created on: 18 dic 2018
 *      Author: daniele_parise
 */

#ifndef SRC_APPLICATION_EXTCOM_HW_EXTCOM_HW_H_
#define SRC_APPLICATION_EXTCOM_HW_EXTCOM_HW_H_



#define USART_EXTCOM                          USART2
#define USART_EXTCOM_CLK_ENABLE()              __USART2_CLK_ENABLE();
#define USART_EXTCOM_RX_GPIO_CLK_ENABLE()      __GPIOA_CLK_ENABLE()
#define USART_EXTCOM_TX_GPIO_CLK_ENABLE()      __GPIOA_CLK_ENABLE()

#define USART_EXTCOM_FORCE_RESET()             __USART2_FORCE_RESET()
#define USART_EXTCOM_RELEASE_RESET()           __USART2_RELEASE_RESET()


#define USART_EXTCOM_TX_PIN                  GPIO_PIN_2
#define USART_EXTCOM_TX_GPIO_PORT            GPIOA
#define USART_EXTCOM_TX_AF                   GPIO_AF4_USART2
#define USART_EXTCOM_RX_PIN                  GPIO_PIN_3
#define USART_EXTCOM_RX_GPIO_PORT            GPIOA
#define USART_EXTCOM_RX_AF                   GPIO_AF4_USART2

#define USART_EXTCOM_WAKEUP_FROM_STOP

/* Definition for USARTx's NVIC */
#define USART_EXTCOM_IRQn                      USART2_IRQn
#define USART_EXTCOM_IRQHandler                USART2_IRQHandler
#define USART_EXTCOM_IRQ_pri					(0x1)

void extcom_hwEnterStopMode(void);
void extcom_hwExitStopMode(void);

#endif /* SRC_APPLICATION_EXTCOM_HW_EXTCOM_HW_H_ */
