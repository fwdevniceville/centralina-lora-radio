/*
 * extcom.c
 *
 *  Created on: 18 dic 2018
 *      Author: daniele_parise
 */
#include <stdbool.h>
#include <stdint.h>
#include "extcom.h"
#include "../extcom_hw/extcom_hw.h"
#include <string.h>
#include "../hwbrd/hw_bridge.h"
#include "../Utils/crc16.h"



GENCOM_CTX	stEXTCOM_CTX;

UART_HandleTypeDef UartExtcomHandle;


void extcom_IoInit(void);
void extcom_IoDeInit(void);

void extcom_Init(gencom_rx_callback_t rxCallback,gencom_txc_callback_t txcCallback)
{
#ifdef USART_EXTCOM_WAKEUP_FROM_STOP
	UART_WakeUpTypeDef			stWakeUpMode = {0};
#endif
	stEXTCOM_CTX.rxCallback			= rxCallback;
	stEXTCOM_CTX.txcCallback		= txcCallback;
	stEXTCOM_CTX.huart				= &UartExtcomHandle;

	UartExtcomHandle.Instance        = USART_EXTCOM;

	UartExtcomHandle.Init.BaudRate   = 38400;
	UartExtcomHandle.Init.WordLength = UART_WORDLENGTH_8B;
	UartExtcomHandle.Init.StopBits   = UART_STOPBITS_2;
	UartExtcomHandle.Init.Parity     = UART_PARITY_NONE;
	UartExtcomHandle.Init.HwFlowCtl  = UART_HWCONTROL_NONE;
	UartExtcomHandle.Init.Mode       = UART_MODE_RX;

#ifdef USART_EXTCOM_INVERT_COMM
	UartExtcomHandle.AdvancedInit.AdvFeatureInit	= UART_ADVFEATURE_TXINVERT_INIT | UART_ADVFEATURE_RXINVERT_INIT;
	UartExtcomHandle.AdvancedInit.TxPinLevelInvert	= UART_ADVFEATURE_TXINV_ENABLE;
	UartExtcomHandle.AdvancedInit.RxPinLevelInvert	= UART_ADVFEATURE_RXINV_ENABLE;
#endif

	gencom_Init(&stEXTCOM_CTX,&UartExtcomHandle,rxCallback,txcCallback);

	HAL_UART_Init(&UartExtcomHandle);

#ifdef USART_EXTCOM_WAKEUP_FROM_STOP
	stWakeUpMode.WakeUpEvent = UART_WAKEUP_ON_STARTBIT;
	HAL_UARTEx_StopModeWakeUpSourceConfig(&UartExtcomHandle,stWakeUpMode);
#endif

//	MODIFY_REG(UartExtcomHandle.Instance->RTOR, USART_RTOR_RTO, 11);
	HAL_NVIC_SetPriority(USART_EXTCOM_IRQn, USART_EXTCOM_IRQ_pri, 0);
	HAL_NVIC_EnableIRQ(USART_EXTCOM_IRQn);

	SET_BIT(UartExtcomHandle.Instance->CR1, USART_CR1_IDLEIE);
	SET_BIT(UartExtcomHandle.Instance->CR1, USART_CR1_RE);
	CLEAR_BIT(UartExtcomHandle.Instance->CR1, USART_CR1_TE);
#ifdef USART_EXTCOM_WAKEUP_FROM_STOP
	SET_BIT(UartExtcomHandle.Instance->CR3, USART_CR3_WUFIE);
#endif

}


void extcom_DeInit(void)
{
	HAL_UART_DeInit(&UartExtcomHandle);
	HAL_NVIC_SetPriority(USART_EXTCOM_IRQn, 0x1, 0);
	HAL_NVIC_DisableIRQ(USART_EXTCOM_IRQn);

	stEXTCOM_CTX.rxCallback	= NULL;
}


void extcom_IoInit(void)
{
	GPIO_InitTypeDef  GPIO_InitStruct={0};
    // Enable GPIO TX/RX clock

	hw_bridge_gpio_clock_enable(USART_EXTCOM_TX_GPIO_PORT,USART_EXTCOM_TX_PIN);
	hw_bridge_gpio_clock_enable(USART_EXTCOM_RX_GPIO_PORT,USART_EXTCOM_RX_PIN);

	// UART TX GPIO pin configuration
	GPIO_InitStruct.Pin       = USART_EXTCOM_TX_PIN;
	GPIO_InitStruct.Mode      = GPIO_MODE_AF_PP;
	GPIO_InitStruct.Speed     = GPIO_SPEED_HIGH;
	GPIO_InitStruct.Alternate = USART_EXTCOM_TX_AF;
#ifdef USART_EXTCOM_INVERT_COMM
	GPIO_InitStruct.Pull      	= GPIO_PULLDOWN;
#else
	GPIO_InitStruct.Pull      	= GPIO_PULLUP;
#endif

	HAL_GPIO_Init(USART_EXTCOM_TX_GPIO_PORT, &GPIO_InitStruct);


	// UART RX GPIO pin configuration
	GPIO_InitStruct.Pin 		= USART_EXTCOM_RX_PIN;
	GPIO_InitStruct.Alternate 	= USART_EXTCOM_RX_AF;

	HAL_GPIO_Init(USART_EXTCOM_RX_GPIO_PORT, &GPIO_InitStruct);
}

void extcom_IoDeInit(void)
{
	GPIO_InitTypeDef GPIO_InitStructure={0};

	hw_bridge_gpio_clock_enable(USART_EXTCOM_TX_GPIO_PORT,USART_EXTCOM_TX_PIN);
	hw_bridge_gpio_clock_enable(USART_EXTCOM_RX_GPIO_PORT,USART_EXTCOM_RX_PIN);

	GPIO_InitStructure.Mode = GPIO_MODE_ANALOG;
	GPIO_InitStructure.Pull = GPIO_NOPULL;

	GPIO_InitStructure.Pin =  USART_EXTCOM_TX_PIN ;
	HAL_GPIO_Init(  USART_EXTCOM_TX_GPIO_PORT, &GPIO_InitStructure );

	GPIO_InitStructure.Pin =  USART_EXTCOM_RX_PIN ;
	HAL_GPIO_Init(  USART_EXTCOM_RX_GPIO_PORT, &GPIO_InitStructure );


}

void extcom_process_lpm_rx(void)
{
	gencom_process_lpm_rx(&stEXTCOM_CTX);
}

void extcom_IRQHandler( void )
{
	gencom_IRQHandler(&stEXTCOM_CTX);
}
