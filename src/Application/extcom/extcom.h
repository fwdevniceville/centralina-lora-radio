/*
 * extcom.h
 *
 *  Created on: 18 dic 2018
 *      Author: daniele_parise
 */

#ifndef SRC_APPLICATION_EXTCOM_EXTCOM_H_
#define SRC_APPLICATION_EXTCOM_EXTCOM_H_

#include "hal_inc.h"

#include "../gencom/gencom.h"


void extcom_Init(gencom_rx_callback_t rxCallback,gencom_txc_callback_t txcCallback);
void extcom_DeInit(void);

void extcom_IoInit(void);
void extcom_IoDeInit(void);

/*
int intcom_Send(unsigned uiCmd,unsigned uiSize, unsigned uiProgr,const uint8_t* uiSendBuffer);
int intcom_Recv(unsigned* puiCmd,unsigned* puiSize, unsigned* puiProgr,uint8_t* pBuffer,unsigned uiBufferSize);
*/
void extcom_process_lpm_rx(void);
void extcom_IRQHandler( void );

extern UART_HandleTypeDef UartExtcomHandle;

extern GENCOM_CTX	stEXTCOM_CTX;


#endif /* SRC_APPLICATION_EXTCOM_EXTCOM_H_ */
