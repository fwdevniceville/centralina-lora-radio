/*
 * modextcom.h
 *
 *  Created on: 20 dic 2018
 *      Author: sa2025
 */

#ifndef SRC_APPLICATION_MODEXTCOM_MODEXTCOM_H_
#define SRC_APPLICATION_MODEXTCOM_MODEXTCOM_H_


#include <gencom/gencom.h>
#include <gencom/gencomhl.h>

void modextcomInit(void);
void modextcomCheck(void);


void modextcomPulsEvent(void);
/*
#define MODINTCOM_CMD_SET_LORA_PARAMS		(0x01)
#define MODINTCOM_CMD_ENABLE_LORA			(0x02)
#define MODINTCOM_CMD_SEND_UPLINK			(0x03)

#define MODINTCOM_CMD_SEND_DOWNLINK			(0x13)
#define MODINTCOM_CMD_SEND_EVENT				(0x14)

#define LORA_USER_EVENT_JOINED			(1)
#define LORA_USER_EVENT_CLASS_CONFIRMED	(2)

#define MODINTCOM_SCHED_SLOT_EVT_JOIN		(0)
#define MODINTCOM_SCHED_SLOT_EVT_CLASS		(1)
#define MODINTCOM_SCHED_SLOT_EVT_DOWNLINK	(2)
*/
extern GENCOMHL_CTX stEXTCOMHL_CTX;


bool modextcom_extcomhlRecvCallback(struct tagGENCOMHL_CTX* pGENCOMHL_CTX,void* pctx,uint8_t uiCmd, unsigned uiSize,unsigned uiProgr,const uint8_t* abyBuffer);
void modextcom_SendCmd_Clbk(struct tagGENCOMHL_CTX* pGENCOMHL_CTX,void* pctx,EGENCOMHL_STATUS_RET eStautsRet,const struct tagGENCOMHL_MESSAGE_REQUEST* pRQST, const struct tagGENCOMHL_MESSAGE_ANSWER* pANSW);
void extcomhlFlushSendRequest(void);


#endif /* SRC_APPLICATION_MODEXTCOM_MODEXTCOM_H_ */
