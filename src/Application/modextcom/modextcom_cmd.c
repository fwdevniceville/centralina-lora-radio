/*
 * modextcom_cmd.c
 *
 *  Created on: 20 dic 2018
 *      Author: daniele_parise
 */



#include <stdbool.h>
#include <stdint.h>

#include <gencom/gencom.h>
#include <gencom/gencomhl.h>
#include "modextcom.h"
#include "../modintcom/modintcom.h"
#include "../msgcom/msgcom.h"
#include "hw.h"
#include <string.h>
#include "lora.h"


void modextcom_extRouteCompleted(void* pctx,EGENCOMHL_STATUS_RET eStautsRet,const struct tagGENCOMHL_MESSAGE_REQUEST* pRQST, const struct tagGENCOMHL_MESSAGE_ANSWER* pANSW,uint8_t uiPROGR_PENDING);

bool modextcom_extcomhlRecvCallback(GENCOMHL_CTX* pGENCOMHL_CTX,void* pctx,uint8_t uiCmd, unsigned uiSize,unsigned uiProgr,const uint8_t* abyBuffer)
{
	uint8_t	auiBufferAnsw[32];
	unsigned uiAnswSize;

	bool bCompleted;
	bCompleted = true;
	uiAnswSize = 2;
	memset(auiBufferAnsw,0,sizeof(auiBufferAnsw));
	auiBufferAnsw[0] =GENCOMHL_ANSW_CMD_UNK;

	if (
			(uiCmd >= MODEXTCOM_CPU_CMD_MIN)
				&&
			(uiCmd <= MODEXTCOM_CPU_CMD_MAX)
		)
	{
		EGENCOMHL_STATUS_RET	eRET;

		eRET = modintcomRouteCmd(uiCmd,uiProgr,uiSize,abyBuffer,modextcom_extRouteCompleted,NULL);

		auiBufferAnsw[0] =GENCOMHL_ANSW_CMD_FAIL;
		switch (eRET)
		{
		default:
			break;
		case GENCOMHL_STATUS_BUSY:
			auiBufferAnsw[0] =GENCOMHL_ANSW_ROUTER_BUSY;
			break;
		case GENCOMHL_STATUS_SCHEDULED:
		case GENCOMHL_STATUS_QUEUED:
		case GENCOMHL_STATUS_SUCCESS:
			auiBufferAnsw[0] =GENCOMHL_ANSW_CMD_WAIT_2ND_RESPONSE;
			break;

		}
	}
	else
	{
		uint32_t	uiTMP;
		switch (uiCmd)
		{
		case MODEXTCOM_RADIO_READ_LORA_PARAMS:
			{
				auiBufferAnsw[0] = GENCOMHL_ANSW_CMD_OK;
				auiBufferAnsw[1] = 0;
				HW_GetUniqueId(&auiBufferAnsw[2]);
				uiTMP = HW_GetRandomSeed();
				auiBufferAnsw[2 + 8 + 0] = uiTMP >> 24;
				auiBufferAnsw[2 + 8 + 1] = uiTMP >> 16;
				auiBufferAnsw[2 + 8 + 2] = uiTMP >> 8;
				auiBufferAnsw[2 + 8 + 3] = uiTMP >> 0;

				uiAnswSize = 2 + 8 + 4;

			}
			break;
		case MODEXTCOM_RADIO_TX_TEST:
			{
				auiBufferAnsw[0] = GENCOMHL_ANSW_CMD_INVALID_PARAMS;
				auiBufferAnsw[1] = 0;
				if (uiSize >= 8)
				{
					LoRaMacStatus_t	ret;
					uint32_t Frequency;
					uint8_t Power;
					uint16_t Timeout;
					Timeout = ( uint16_t )( (abyBuffer[0] << 8) | abyBuffer[1]);
					Power = ( uint16_t )( (abyBuffer[2] << 8) | abyBuffer[3]);
					Frequency = ( uint32_t )( (abyBuffer[4] << 24) | (abyBuffer[5] << 16) | (abyBuffer[6] << 8) | abyBuffer[7]);

					ret = LORA_TXContinuosWave(Frequency,Power,Timeout);

					uiAnswSize = 2;
					auiBufferAnsw[0] = ret;
					auiBufferAnsw[1] = 0;

				}
			}
			break;
		case MODEXTCOM_RADIO_CPU_UPDATE_COMMANDS:
		case MODEXTCOM_RADIO_CPU_REBOOT:
			{
			}
			break;
		}
	}
	gencomhlPostRecvAnswer(pGENCOMHL_CTX,uiCmd,uiAnswSize, uiProgr,auiBufferAnsw);

	return bCompleted;
}

void modextcom_extRouteCompleted(void* pctx,EGENCOMHL_STATUS_RET eStautsRet,const struct tagGENCOMHL_MESSAGE_REQUEST* pRQST, const struct tagGENCOMHL_MESSAGE_ANSWER* pANSW,uint8_t uiPROGR_PENDING)
{
	if (eStautsRet == GENCOMHL_STATUS_SUCCESS)
	{
		gencomhlScheduleSendRequest(
				&stEXTCOMHL_CTX,
				0,
				uiPROGR_PENDING,
				pANSW->uiCODE,
				pANSW->uiSIZE,
				pANSW->aBuffer,
				0,
				NULL,
				0,
				0);
	}
	//gencomhlScheduleSendRequest(&stEXTCOMHL_CTX,,pANSW)
}


