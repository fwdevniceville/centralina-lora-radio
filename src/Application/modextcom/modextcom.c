/*
 * modextcom.c
 *
 *  Created on: 20 dic 2018
 *      Author: sa2025
 */


#include <stdbool.h>
#include <stdint.h>
#include <gencom/gencom.h>
#include <gencom/gencomhl.h>

#include "../modextcom/modextcom.h"
#include "../extcom/extcom.h"
#include "timeServer.h"
#include "hw.h"

#include "low_power_manager.h"

typedef struct tagMODEXTCOMCTX{
	GENCOMHL_REQUEST_SENDER_CTX	stSEND_REQUESTS;
	uint32_t					uiTms_last;
	unsigned					uiEVT_MASK;
	bool						bInSlotProcessing;
}MODEXTCOMCTX;

MODEXTCOMCTX	stMODEXTCOMCTX;
GENCOMHL_CTX stEXTCOMHL_CTX;




typedef struct tagMODEXTCOM_ANSW_ROUTE{
	GENCOMHL_RQST_HDR	stHDR;
	uint8_t				auiPayLoad[512];
}MODEXTCOM_ANSW_ROUTE;


MODEXTCOM_ANSW_ROUTE	stMODEXTCOM_ANSW_ROUTE;

const GENCOMHL_RQST_BLOCK aMODEXTOM_RQSTS[] = {
		{&stMODEXTCOM_ANSW_ROUTE.stHDR,sizeof(stMODEXTCOM_ANSW_ROUTE.auiPayLoad)}
};


TimerEvent_t timerModExtCom;
void OnTimerModExtComEvent(  void* context  );


bool modextcomOnBusRxCallBack(GENCOM_CTX* pGENCOM_CTX,uint8_t uiCmd, unsigned uiSize,unsigned uiProgr,const uint8_t* abyBuffer);
bool modextcomOnBusTxcCallBack(GENCOM_CTX* pGENCOM_CTX,EGENCOM_SEND_REASON eReason,uint8_t uiCmd, unsigned ,unsigned uiProgr,const uint8_t* abyBuffer);

void modextcomInit(void)
{
	stMODEXTCOMCTX.stSEND_REQUESTS.aRQSTS 		= &aMODEXTOM_RQSTS[0];
	stMODEXTCOMCTX.stSEND_REQUESTS.uiRequestMax = sizeof(aMODEXTOM_RQSTS) / sizeof(aMODEXTOM_RQSTS[0]);

	extcom_Init(modextcomOnBusRxCallBack,modextcomOnBusTxcCallBack);
	gencomhlInit(&stEXTCOMHL_CTX,&stEXTCOM_CTX,modextcom_extcomhlRecvCallback,modextcom_SendCmd_Clbk,NULL,&stMODEXTCOMCTX.stSEND_REQUESTS);


	TimerInit( &timerModExtCom, OnTimerModExtComEvent );

}

void modextcomCheck(void)
{

}


bool modextcomOnBusRxCallBack(GENCOM_CTX* pGENCOM_CTX,uint8_t uiCmd, unsigned uiSize,unsigned uiProgr,const uint8_t* abyBuffer)
{
	unsigned uxBitsToSet;
	bool		bValid;
	bool		bIsAck;
	uxBitsToSet = 0;
	bIsAck = false;
	BACKUP_PRIMASK();
	DISABLE_IRQ( );


	bValid = gencomhlCheckAndStoreRecvFromBus(&stEXTCOMHL_CTX,uiCmd,uiSize,uiProgr,abyBuffer,&bIsAck);
	if (bValid)
	{
		if (bIsAck)
		{
			uxBitsToSet = GENCOMHL_EVENT_ANSW_RECV;

		}
		else
		{
			uxBitsToSet = GENCOMHL_EVENT_RQST_RECV;
		}
	}

	bValid = false;
	if (uxBitsToSet != 0)
	{
		stMODEXTCOMCTX.uiEVT_MASK |= uxBitsToSet;
		TimerStop(&timerModExtCom);
//		TimerSetValue(&timerModCom,1);
//		TimerStart(&timerModCom);
		// acceleriamo
		OnTimerModExtComEvent(NULL);
		bValid = true;
	}
    RESTORE_PRIMASK( );

	return bValid;
}

bool modextcomOnBusTxcCallBack(GENCOM_CTX* pGENCOM_CTX,EGENCOM_SEND_REASON eReason,uint8_t uiCmd, unsigned uiSize,unsigned uiProgr,const uint8_t* abyBuffer)
{
	unsigned uxBitsToSet;
	bool		bRet;
	BACKUP_PRIMASK();
	DISABLE_IRQ( );

	uxBitsToSet = GENCOMHL_EVENT_RQST_SENT;
	switch (eReason)
	{
	default:
	case GENCOM_SEND_MESSAGE:
		uxBitsToSet = GENCOMHL_EVENT_RQST_SENT;
		break;
	case GENCOM_SEND_ROUTED_ANSWER:
		uxBitsToSet = GENCOMHL_EVENT_ROUT_ANSW_SENT;
		break;
	case GENCOM_SEND_ANSWER:
		uxBitsToSet = GENCOMHL_EVENT_ANSW_SENT;
		break;
	}

	bRet = false;
	if (uxBitsToSet != 0)
	{
		stMODEXTCOMCTX.uiEVT_MASK |= uxBitsToSet;
		TimerStop(&timerModExtCom);
//		TimerSetValue(&timerModCom,1);
//		TimerStart(&timerModCom);
		// acceleriamo
		OnTimerModExtComEvent(NULL);
		bRet = true;
	}
    RESTORE_PRIMASK( );
	return bRet;
}


void extcomhlFlushSendRequest(void)
{
	OnTimerModExtComEvent(NULL);
}

void OnTimerModExtComEvent(  void* context  )
{
	uint32_t	uiTTicks;
	uint32_t	uiTms_now;
	uint32_t	uiTms_delta;
	int			iNextDelta;
	int			iIterMax;
	bool		bSysRunning;

	if (stMODEXTCOMCTX.bInSlotProcessing)
	{
		return;
	}

	BACKUP_PRIMASK();
	DISABLE_IRQ( );
	if (!stMODEXTCOMCTX.bInSlotProcessing)
	{
		stMODEXTCOMCTX. bInSlotProcessing = true;


		uiTTicks = HW_RTC_GetTimerValue();
		uiTms_now = HW_RTC_Tick2ms(uiTTicks);

		uiTms_delta = uiTms_now - stMODEXTCOMCTX.uiTms_last;
		stMODEXTCOMCTX.uiTms_last = uiTms_now;

		iIterMax = 10;

		do
		{
			bSysRunning = false;
			iNextDelta = gencomhlExecComunicationSlot(&stEXTCOMHL_CTX,uiTms_delta,stMODEXTCOMCTX.uiEVT_MASK,&bSysRunning);
			uiTms_delta = 0;
			stMODEXTCOMCTX.uiEVT_MASK = 0;
			iIterMax--;
		}
		while ((iNextDelta == 0) && (iIterMax > 0));

		LPM_SetStopMode(LPM_EXTCOMHL_Id , bSysRunning ? LPM_Disable : LPM_Enable );

		TimerStop(&timerModExtCom);
		if (iNextDelta >= 0)
		{
			TimerSetValue(&timerModExtCom,iNextDelta);
			TimerStart(&timerModExtCom);
		}

		stMODEXTCOMCTX. bInSlotProcessing = false;
	}
	RESTORE_PRIMASK( );
}

EGENCOMHL_STATUS_RET modextcom_SendCmd(uint8_t uiCmd, unsigned uiSize,const uint8_t* abyBuffer,uint32_t uiTimeOut);
EGENCOMHL_STATUS_RET modextcom_SendCmd(uint8_t uiCmd, unsigned uiSize,const uint8_t* abyBuffer,uint32_t uiTimeOut)
{

	EGENCOMHL_STATUS_RET eRet;
	eRet = GENCOMHL_STATUS_INVAL;

	if (uiSize <= GENCOMHL_REQUEST_MAX)
	{

//		osMutexWait(mutexIntComHandle, osWaitForever);
		BACKUP_PRIMASK();
		DISABLE_IRQ( );


		eRet = gencomhlPostSendRequest(&stEXTCOMHL_CTX,0,uiCmd,uiSize,abyBuffer,0,NULL,uiTimeOut,3);
		if (eRet == GENCOMHL_STATUS_SUCCESS)
		{
			TimerStop(&timerModExtCom);
			TimerSetValue(&timerModExtCom,1);
			TimerStart(&timerModExtCom);
		}
	    RESTORE_PRIMASK( );
//		osMutexRelease(mutexIntComHandle);
	}
	return eRet;
}

void modextcom_SendCmd_Clbk(GENCOMHL_CTX* pGENCOMHL_CTX,void* pctx,EGENCOMHL_STATUS_RET eStautsRet,const struct tagGENCOMHL_MESSAGE_REQUEST* pRQST, const struct tagGENCOMHL_MESSAGE_ANSWER* pANSW)
{
//	osMutexWait(mutexIntComHandle, portMAX_DELAY);
	EGENCOMHL_STATUS_RET	eRet;
	uint8_t uiRetStatus;
	unsigned uiRetSize;
	uint8_t abyRetBuffer[128];
	bool	bCancelled;
	bCancelled = false;

	BACKUP_PRIMASK();
	DISABLE_IRQ( );

	eRet = gencomhlCompleteSendRequest(pGENCOMHL_CTX,&uiRetStatus,&uiRetSize,abyRetBuffer,bCancelled);
	if (eRet == GENCOMHL_STATUS_SUCCESS)
	{
	}


	// next request.............
	gencomhlCheckAndSendPendingRequest(pGENCOMHL_CTX);


    RESTORE_PRIMASK( );

//	xEventGroupSetBits(evtgrpIntComFunctionsCompletedHandle,TASKINCOM_EVENT_FUNCTION_COMPLETED_SEND);
}
