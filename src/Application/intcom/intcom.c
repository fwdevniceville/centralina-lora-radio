#include <stdbool.h>
#include <stdint.h>
#include "intcom.h"
#include "../gencom/gencom.h"
#include "../intcom_hw/intcom_hw.h"
#include <string.h>
#include "../hwbrd/hw_bridge.h"
#include "../Utils/crc16.h"


GENCOM_CTX	stINTCOM_CTX;

UART_HandleTypeDef UartIntcomHandle;



void intcom_IoInit(void);
void intcom_IoDeInit(void);



void intcom_Init(gencom_rx_callback_t rxCallback,gencom_txc_callback_t txcCallback)
{
#ifdef USART_INTCOM_WAKEUP_FROM_STOP
	UART_WakeUpTypeDef			stWakeUpMode = {0};
#endif

	UartIntcomHandle.Instance        = USART_INTCOM;
  
	UartIntcomHandle.Init.BaudRate   = 28800;
	UartIntcomHandle.Init.WordLength = UART_WORDLENGTH_8B;
	UartIntcomHandle.Init.StopBits   = UART_STOPBITS_2;
	UartIntcomHandle.Init.Parity     = UART_PARITY_NONE;
	UartIntcomHandle.Init.HwFlowCtl  = UART_HWCONTROL_NONE;
	UartIntcomHandle.Init.Mode       = UART_MODE_RX;

#ifdef USART_INTCOM_INVERT_COMM
	UartIntcomHandle.AdvancedInit.AdvFeatureInit	= UART_ADVFEATURE_TXINVERT_INIT | UART_ADVFEATURE_RXINVERT_INIT;
	UartIntcomHandle.AdvancedInit.TxPinLevelInvert	= UART_ADVFEATURE_TXINV_ENABLE;
	UartIntcomHandle.AdvancedInit.RxPinLevelInvert	= UART_ADVFEATURE_RXINV_ENABLE;
#endif

	gencom_Init(&stINTCOM_CTX,&UartIntcomHandle,rxCallback,txcCallback);

	HAL_UART_Init(&UartIntcomHandle);

#ifdef USART_INTCOM_WAKEUP_FROM_STOP
	stWakeUpMode.WakeUpEvent = UART_WAKEUP_ON_STARTBIT;
	HAL_UARTEx_StopModeWakeUpSourceConfig(&UartIntcomHandle,stWakeUpMode);
#endif

//	MODIFY_REG(UartIntcomHandle.Instance->RTOR, USART_RTOR_RTO, 11);
	HAL_NVIC_SetPriority(USART_INTCOM_IRQn, USART_INTCOM_IRQ_pri, 0);
	HAL_NVIC_EnableIRQ(USART_INTCOM_IRQn);

	SET_BIT(UartIntcomHandle.Instance->CR1, USART_CR1_IDLEIE);
	SET_BIT(UartIntcomHandle.Instance->CR1, USART_CR1_RE);
	CLEAR_BIT(UartIntcomHandle.Instance->CR1, USART_CR1_TE);
#ifdef USART_INTCOM_WAKEUP_FROM_STOP
	SET_BIT(UartIntcomHandle.Instance->CR3, USART_CR3_WUFIE);
#endif

}


void intcom_DeInit(void)
{
	HAL_UART_DeInit(&UartIntcomHandle);
	HAL_NVIC_SetPriority(USART_INTCOM_IRQn, 0x1, 0);
	HAL_NVIC_DisableIRQ(USART_INTCOM_IRQn);

	gencom_DeInit(&stINTCOM_CTX);

}


void intcom_IoInit(void)
{
	GPIO_InitTypeDef  GPIO_InitStruct={0};
    // Enable GPIO TX/RX clock

	hw_bridge_gpio_clock_enable(USART_INTCOM_TX_GPIO_PORT,USART_INTCOM_TX_PIN);
	hw_bridge_gpio_clock_enable(USART_INTCOM_RX_GPIO_PORT,USART_INTCOM_RX_PIN);

	// UART TX GPIO pin configuration
	GPIO_InitStruct.Pin       = USART_INTCOM_TX_PIN;
	GPIO_InitStruct.Mode      = GPIO_MODE_AF_PP;
#ifdef USART_INTCOM_INVERT_COMM
	GPIO_InitStruct.Pull      = GPIO_PULLDOWN;
#else
	GPIO_InitStruct.Pull      = GPIO_PULLUP;
#endif

	GPIO_InitStruct.Speed     = GPIO_SPEED_HIGH;
	GPIO_InitStruct.Alternate = USART_INTCOM_TX_AF;

	HAL_GPIO_Init(USART_INTCOM_TX_GPIO_PORT, &GPIO_InitStruct);

	// UART RX GPIO pin configuration
	GPIO_InitStruct.Pin 		= USART_INTCOM_RX_PIN;
	GPIO_InitStruct.Alternate 	= USART_INTCOM_RX_AF;

	HAL_GPIO_Init(USART_INTCOM_RX_GPIO_PORT, &GPIO_InitStruct);
}

void intcom_IoDeInit(void)
{
	GPIO_InitTypeDef GPIO_InitStructure={0};

	hw_bridge_gpio_clock_enable(USART_INTCOM_TX_GPIO_PORT,USART_INTCOM_TX_PIN);
	hw_bridge_gpio_clock_enable(USART_INTCOM_RX_GPIO_PORT,USART_INTCOM_RX_PIN);

	GPIO_InitStructure.Mode = GPIO_MODE_ANALOG;
	GPIO_InitStructure.Pull = GPIO_NOPULL;

	GPIO_InitStructure.Pin =  USART_INTCOM_TX_PIN ;
	HAL_GPIO_Init(  USART_INTCOM_TX_GPIO_PORT, &GPIO_InitStructure );

	GPIO_InitStructure.Pin =  USART_INTCOM_RX_PIN ;
	HAL_GPIO_Init(  USART_INTCOM_RX_GPIO_PORT, &GPIO_InitStructure );

}

void intcom_process_lpm_rx(void)
{
	gencom_process_lpm_rx(&stINTCOM_CTX);
}

void intcom_IRQHandler( void )
{
	gencom_IRQHandler(&stINTCOM_CTX);
}




/*
void intcom_Print( void)
{
  char* CurChar;
  while( ( (iw+BUFSIZE-ir)%BUFSIZE) >0 )
  {
    BACKUP_PRIMASK();
    DISABLE_IRQ();

    CurChar = &buff[ir];
    ir= (ir+1) %BUFSIZE;

    RESTORE_PRIMASK();

    HAL_UART_Transmit(&UartHandle,(uint8_t *) CurChar, 1, 300);
  }
  HAL_NVIC_ClearPendingIRQ(USARTX_IRQn);
}

void intcom_Send_Lp( char *format, ... )
{
  va_list args;
  va_start(args, format);
  uint8_t len;
  uint8_t lenTop;
  char tempBuff[128];

  BACKUP_PRIMASK();
  DISABLE_IRQ();

  len = vsprintf(&tempBuff[0], format, args);

  if (iw+len<BUFSIZE)
  {
    memcpy( &buff[iw], &tempBuff[0], len);
    iw+=len;
  }
  else
  {
    lenTop=BUFSIZE-iw;
    memcpy( &buff[iw], &tempBuff[0], lenTop);
    len-=lenTop;
    memcpy( &buff[0], &tempBuff[lenTop], len);
    iw = len;
  }
  RESTORE_PRIMASK();

  va_end(args);
}
*/
/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/

#if 0
void intcom_IRQHandler_rx_error()

	  if (errorflags == RESET)
	  {
	    // UART in mode Receiver ---------------------------------------------------
	    if(((isrflags & USART_ISR_RXNE) != RESET) && ((cr1its & USART_CR1_RXNEIE) != RESET))
	    {
	      UART_Receive_IT(huart);
	      return;
	    }
	  }

// If some errors occur
	  cr3its = READ_REG(huart->Instance->CR3);
	  if(   (errorflags != RESET)
	     && (   ((cr3its & USART_CR3_EIE) != RESET)
	         || ((cr1its & (USART_CR1_RXNEIE | USART_CR1_PEIE)) != RESET)) )
	  {
		  // UART parity error interrupt occurred -------------------------------------
	    if(((isrflags & USART_ISR_PE) != RESET) && ((cr1its & USART_CR1_PEIE) != RESET))
	    {
	      __HAL_UART_CLEAR_IT(huart, UART_CLEAR_PEF);

	      huart->ErrorCode |= HAL_UART_ERROR_PE;
	    }

	    // UART frame error interrupt occurred --------------------------------------
	    if(((isrflags & USART_ISR_FE) != RESET) && ((cr3its & USART_CR3_EIE) != RESET))
	    {
	      __HAL_UART_CLEAR_IT(huart, UART_CLEAR_FEF);

	      huart->ErrorCode |= HAL_UART_ERROR_FE;
	    }

	    // UART noise error interrupt occurred --------------------------------------
	    if(((isrflags & USART_ISR_NE) != RESET) && ((cr3its & USART_CR3_EIE) != RESET))
	    {
	      __HAL_UART_CLEAR_IT(huart, UART_CLEAR_NEF);

	      huart->ErrorCode |= HAL_UART_ERROR_NE;
	    }

	    // UART Over-Run interrupt occurred -----------------------------------------
	    if(((isrflags & USART_ISR_ORE) != RESET) &&
	       (((cr1its & USART_CR1_RXNEIE) != RESET) || ((cr3its & USART_CR3_EIE) != RESET)))
	    {
	      __HAL_UART_CLEAR_IT(huart, UART_CLEAR_OREF);

	      huart->ErrorCode |= HAL_UART_ERROR_ORE;
	    }

	    // Call UART Error Call back function if need be --------------------------
	    if(huart->ErrorCode != HAL_UART_ERROR_NONE)
	    {
	      // UART in mode Receiver ---------------------------------------------------
	      if(((isrflags & USART_ISR_RXNE) != RESET) && ((cr1its & USART_CR1_RXNEIE) != RESET))
	      {
	        UART_Receive_IT(huart);
	      }

	      // If Overrun error occurs, or if any error occurs in DMA mode reception,
	         consider error as blocking
	      if (((huart->ErrorCode & HAL_UART_ERROR_ORE) != RESET) ||
	          (HAL_IS_BIT_SET(huart->Instance->CR3, USART_CR3_DMAR)))
	      {
	        // Blocking error : transfer is aborted
	           Set the UART state ready to be able to start again the process,
	           Disable Rx Interrupts, and disable Rx DMA request, if ongoing
	        UART_EndRxTransfer(huart);

	        // Disable the UART DMA Rx request if enabled
	        if (HAL_IS_BIT_SET(huart->Instance->CR3, USART_CR3_DMAR))
	        {
	          CLEAR_BIT(huart->Instance->CR3, USART_CR3_DMAR);

	          // Abort the UART DMA Rx channel
	          if(huart->hdmarx != NULL)
	          {
	            // Set the UART DMA Abort callback :
	               will lead to call HAL_UART_ErrorCallback() at end of DMA abort procedure
	            huart->hdmarx->XferAbortCallback = UART_DMAAbortOnError;

	            // Abort DMA RX
	            if(HAL_DMA_Abort_IT(huart->hdmarx) != HAL_OK)
	            {
	              // Call Directly huart->hdmarx->XferAbortCallback function in case of error
	              huart->hdmarx->XferAbortCallback(huart->hdmarx);
	            }
	          }
	          else
	          {
	            // Call user error callback
	            HAL_UART_ErrorCallback(huart);
	          }
	        }
	        else
	        {
	          // Call user error callback
	          HAL_UART_ErrorCallback(huart);
	        }
	      }
	      else
	      {
	        // Non Blocking error : transfer could go on.
	           Error is notified to user through user error callback
	        HAL_UART_ErrorCallback(huart);
	        huart->ErrorCode = HAL_UART_ERROR_NONE;
	      }
	    }
	    return;

	  } // End if some error occurs

	  // UART wakeup from Stop mode interrupt occurred ---------------------------
	  if(((isrflags & USART_ISR_WUF) != RESET) && ((cr3its & USART_CR3_WUFIE) != RESET))
	  {
	    __HAL_UART_CLEAR_IT(huart, UART_CLEAR_WUF);
	    // Set the UART state ready to be able to start again the process
	    huart->gState  = HAL_UART_STATE_READY;
	    huart->RxState = HAL_UART_STATE_READY;
	    HAL_UARTEx_WakeupCallback(huart);
	    return;
	  }


#endif

