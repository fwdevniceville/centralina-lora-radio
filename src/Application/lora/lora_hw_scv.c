/*
 * lora_hw_scv.c
 *
 *  Created on: 12 mar 2018
 *      Author: daniele_parise
 */
#include <stdbool.h>
#include <stdint.h>


#include "lora_hw_scv.h"
#include "hal_inc.h"



LORA_HW_SVC_DEV_INFOS	stLORA_HW_SVC_DEV_INFOS;
uint16_t lora_hw_svc_TemperatureLevel( void )
{
	return stLORA_HW_SVC_DEV_INFOS.uiTemperature;
}
// @retval the battery level  1 (very low) to 254 (fully charged)

uint8_t lora_hw_svc_GetBatteryLevel( void )
{
	return stLORA_HW_SVC_DEV_INFOS.uiBatteryLevel;
}
