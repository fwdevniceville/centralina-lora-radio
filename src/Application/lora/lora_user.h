/*
 * lora_user.h
 *
 *  Created on: 12 mar 2018
 *      Author: daniele_parise
 */

#ifndef SRC_APPLICATION_LORA_LORA_USER_H_
#define SRC_APPLICATION_LORA_LORA_USER_H_

#include "lora.h"


typedef struct tagLORAWAN_PARAMS{
	uint8_t		uiEnableAdaptiveDataRate;
	int8_t		iTransmissionDataRate;
	uint8_t		uiEnablePublicNetwork;
	uint8_t		uiJointRequestTrials;


	uint8_t		uiABP;

	uint8_t		auiABP_DEV_ADDRESS[4];
	uint8_t		auiABP_NWK_ID[4];

	uint8_t		auiDEV_EUI[8];
	uint8_t		auiJOIN_EUI[8];

	uint8_t		auiAPP_KEY__ABP_S_KEY[16];
	uint8_t		auiNWK_KEY__ABP_S_KEY[16];

	uint8_t		auiABP_S_NWK_S_INT_KEY[16];
	uint8_t		auiABP_F_NWK_S_INT_KEY[16];
}LORAWAN_PARAMS;


void lora_user_Init(void);

#ifdef MAIN_STANDALONE
void lora_user_force_ok(void);
#endif

#define LORAWAN_STATUS_OK				(0x00)
#define LORAWAN_STATUS_NJOIN			(0x40)
#define LORAWAN_STATUS_NINIT			(0x41)
#define LORAWAN_STATUS_NENABLE			(0x42)

#define LORAWAN_STATUS_INVAL						(0x81)
#define LORAWAN_STATUS_GENERIC_LORAMAC_FAULT		(0x90)
#define LORAWAN_MAC_STATUS_DUTY_VIOLATION			(0x91)
#define LORAWAN_MAC_STATUS_SERVICE_UNKNOWN			(0x92)
#define LORAWAN_MAC_STATUS_PARAMETER_INVALID		(0x93)
#define LORAWAN_MAC_STATUS_FREQUENCY_INVALID		(0x94)
#define LORAWAN_MAC_STATUS_DATARATE_INVALID			(0x95)
#define LORAWAN_MAC_STATUS_FREQ_AND_DR_INVALID		(0x96)
#define LORAWAN_MAC_STATUS_NO_NETWORK_JOINED		(0x97)
#define LORAWAN_MAC_STATUS_LENGTH_ERROR				(0x98)
#define LORAWAN_MAC_STATUS_DEVICE_OFF				(0x99)
#define LORAWAN_MAC_STATUS_REGION_NOT_SUPPORTED		(0x9A)
#define LORAWAN_MAC_STATUS_CERTIFICATION_RUNNING	(0x9B)
#define LORAWAN_MAC_STATUS_NO_CHANNEL_FOUND			(0x9C)
#define LORAWAN_MAC_STATUS_TX_SYS_BUSY				(0x9D)

void lora_user_init_defaults(void);
bool lora_userInitIsOK(void);
uint8_t lora_userSetParams(const LORAWAN_PARAMS* pLoRaWanParams);
struct tagLoRaMacCurrentTxInfos;
uint8_t lora_userJoin(uint8_t uiBaseDataRate, struct tagLoRaMacCurrentTxInfos* pLoRaMacCurrentTxInfos );
uint8_t lora_user_SendData(unsigned uiPort, bool bConfirmNeeded, unsigned uiSize, const uint8_t* abyBuffer ,  struct tagLoRaMacCurrentTxInfos* pLoRaMacCurrentTxInfos ,int* peLoRaMacStatus);


#define LORA_USER_EVENT_REBOOT			(0)
#define LORA_USER_EVENT_JOINED			(1)
#define LORA_USER_EVENT_CLASS_CONFIRMED	(2)
void lora_user_OnEvent(uint8_t uiEvent,uint8_t uiData);

typedef struct tagLoraUserRxInfos{
	uint32_t uiDownLinkCounter;
	int16_t Rssi;
	uint8_t Snr;
	uint8_t	bFramePending;
}LoraUserRxInfos;

void lora_user_OnRxData(const LoraUserRxInfos* pInfos, uint8_t Port,uint8_t BuffSize,uint8_t* Buff);


#define LORA_USER_TR_BUFFER_MAX	(256)

typedef struct tagLORA_USER_CTX{
	LORAWAN_PARAMS	stLoRaWANParams;
	struct {
		uint8_t			abyBufferRX[LORA_USER_TR_BUFFER_MAX];
		uint8_t			abyBufferTX[LORA_USER_TR_BUFFER_MAX];
		lora_AppData_t	stRxSlot;
		lora_AppData_t	stTxSlot;
		bool			bRxPendind;
		bool			bTxPendind;
	}stTRANSMITION;
	bool				bInitOK;
	bool				bEnabledOK;
	bool				bJoinedOK;
	bool				bLoraMACProcRqst;
}LORA_USER_CTX;

extern LORA_USER_CTX	stLORA_USER_CTX;


#endif /* SRC_APPLICATION_LORA_LORA_USER_H_ */
