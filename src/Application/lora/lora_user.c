/*
 * lora_user.c
 *
 *  Created on: 12 mar 2018
 *      Author: daniele_parise
 */
#include <stdbool.h>
#include <stdint.h>
#include <stddef.h>

#include "lora_hw_scv.h"
#include "lora_user.h"
#include "lora_user_def.h"

#include "low_power_manager.h"

#include "hw.h"
#include "hw_rtc.h"
#include "debug.h"
#include "Utils/chksum.h"

#include "lora.h"
#include "../../hw/debug.h"
uint32_t lora_user_GetRandomSeed(void);
void lora_user_GetUniqueId( uint8_t *id );

uint32_t lora_user_GetRandomSeed(void);
void lora_user_GetJoinData(
	uint8_t *DevEui,
	uint8_t *JoinEui,
	uint8_t *AppKey,
	uint8_t *NwkKey,
	uint32_t* DevAddr,
	uint32_t* NetId,
	uint8_t *AppSKey,
	uint8_t *NwkSEncKey,
	uint8_t *FNwkSIntKey,
	uint8_t *SNwkSIntKey
	);

void lora_user_internal_OnRxData( lora_AppData_t *AppData, const McpsIndication_t *McpsIndication );
void lora_user_internal_OnHasJoined( void );
void lora_user_internal_OnConfirmClass ( DeviceClass_t Class );
void lora_user_internal_OnTxNeeded ( void );
void lora_user_internal_OnMacProcessNotify ( void );

void lora_user_internal_OnMacMcpsIndicationAll ( const McpsIndication_t *McpsIndication );

#define LORA_USER_EVENT_JOINED			(1)
#define LORA_USER_EVENT_CLASS_CONFIRMED	(2)
void lora_user_OnEvent(uint8_t uiEvent,uint8_t uiData);


//void lora_user_
void lora_user_internal_OnRxData( lora_AppData_t *AppData, const McpsIndication_t *McpsIndication )
{
	PRINTF("MAC_INDICATION DOWNLINK (seqRx= %d) FramePending = %d , rssi = %d, snr = %d, sts= %d\n\r",
			McpsIndication->DownLinkCounter,
			McpsIndication->FramePending,
			McpsIndication->Rssi,
			McpsIndication->Snr,
			McpsIndication->Status
			);

	LoraUserRxInfos	stLoraUserRxInfos;
	stLoraUserRxInfos.uiDownLinkCounter = McpsIndication->DownLinkCounter;
	stLoraUserRxInfos.bFramePending 	= McpsIndication->FramePending;
	stLoraUserRxInfos.Rssi 				= McpsIndication->Rssi;
	stLoraUserRxInfos.Snr 				= McpsIndication->Snr;

	lora_user_OnRxData(&stLoraUserRxInfos,AppData->Port,AppData->BuffSize,AppData->Buff);
}


void lora_user_internal_OnHasJoined( void )
{
	PRINTF("JOINED\n\r");
	LORA_RequestClass( LORAWAN_DEFAULT_CLASS );
	lora_user_OnEvent(LORA_USER_EVENT_JOINED,LORAWAN_DEFAULT_CLASS);
}

void lora_user_internal_OnConfirmClass ( DeviceClass_t Class )
{
	PRINTF("switch to class %c done\n\r","ABC"[Class] );
	lora_user_SendData(LORAWAN_APP_PORT,0,false,NULL , NULL,NULL);
	lora_user_OnEvent(LORA_USER_EVENT_CLASS_CONFIRMED,Class);
}

void lora_user_internal_OnMacMcpsIndicationOthers ( const McpsIndication_t *mcpsIndication )
{
	PRINTF("MAC_INDICATION OTH (seqRx= %d) FramePending = %d , rssi = %d, snr = %d, sts= %d\n\r",
			mcpsIndication->DownLinkCounter,
			mcpsIndication->FramePending,
			mcpsIndication->Rssi,
			mcpsIndication->Snr,
			mcpsIndication->Status
			);
	LoraUserRxInfos	stLoraUserRxInfos;
	stLoraUserRxInfos.uiDownLinkCounter = mcpsIndication->DownLinkCounter;
	stLoraUserRxInfos.bFramePending 	= mcpsIndication->FramePending;
	stLoraUserRxInfos.Rssi 				= mcpsIndication->Rssi;
	stLoraUserRxInfos.Snr 				= mcpsIndication->Snr;

	lora_user_OnRxData(&stLoraUserRxInfos,0,0,NULL);

//	FUNZIONA????
}


LORA_USER_CTX	stLORA_USER_CTX;

#ifdef MAIN_STANDALONE
void lora_user_force_ok(void)
{
	stLORA_USER_CTX.bInitOK 	= true;
	stLORA_USER_CTX.bEnabledOK 	= true;
	stLORA_USER_CTX.bJoinedOK 	= true;
}
#endif

void lora_user_Init(void)
{
	stLORA_USER_CTX.stTRANSMITION.stRxSlot.Buff = &stLORA_USER_CTX.stTRANSMITION.abyBufferRX[0];
	stLORA_USER_CTX.stTRANSMITION.stTxSlot.Buff = &stLORA_USER_CTX.stTRANSMITION.abyBufferTX[0];

	stLORA_USER_CTX.bInitOK 	= false;
	stLORA_USER_CTX.bEnabledOK 	= false;
	stLORA_USER_CTX.bJoinedOK 	= false;
}


LoRaMainCallback_t LoRaMainCallbacks =
{
		lora_hw_svc_GetBatteryLevel,
		lora_hw_svc_TemperatureLevel,
		lora_user_GetJoinData,
		lora_user_GetRandomSeed,
		lora_user_internal_OnRxData,
		lora_user_internal_OnHasJoined,
		lora_user_internal_OnConfirmClass,
		lora_user_internal_OnTxNeeded,
		lora_user_internal_OnMacProcessNotify,
		lora_user_internal_OnMacMcpsIndicationOthers
};

LoRaParam_t LoRaParam;

void lora_user_internal_OnMacProcessNotify ( void )
{
	stLORA_USER_CTX.bLoraMACProcRqst = true;
}

void lora_user_internal_OnTxNeeded ( void )
{
	/*
  AppData.BuffSize = 0;
  AppData.Port = LORAWAN_APP_PORT;

  LORA_send( &AppData, LORAWAN_UNCONFIRMED_MSG);
 */
}

LoRaParam_t LoRaParamInitDefault=
{
		LORAWAN_ADR_STATE,
		LORAWAN_DEFAULT_DATA_RATE,
		LORAWAN_PUBLIC_NETWORK
};

void lora_user_init_defaults(void)
{
	LORA_Init( &LoRaMainCallbacks, &LoRaParamInitDefault);
}

uint8_t lora_userSetParams(const LORAWAN_PARAMS* pLoRaWanParams)
{
	stLORA_USER_CTX.stLoRaWANParams 	= *pLoRaWanParams;
	LoRaParam.AdrEnable				= pLoRaWanParams->uiEnableAdaptiveDataRate;
	LoRaParam.TxDatarate			= pLoRaWanParams->iTransmissionDataRate;
	LoRaParam.EnablePublicNetwork	= pLoRaWanParams->uiEnablePublicNetwork;

	LORA_Init( &LoRaMainCallbacks, &LoRaParam);

	stLORA_USER_CTX.bInitOK 	= true;
	stLORA_USER_CTX.bEnabledOK 	= false;
	stLORA_USER_CTX.bJoinedOK 	= false;

	return LORAWAN_STATUS_OK;

}

bool lora_userInitIsOK(void)
{
	return stLORA_USER_CTX.bInitOK;
}

uint32_t HW_GetRandomSeed(void);

uint32_t lora_user_GetRandomSeed(void)
{
	return HW_GetRandomSeed();
}

void HW_GetUniqueId( uint8_t *id );

void lora_user_GetJoinData(
	uint8_t *pDevEui,
	uint8_t *pJoinEui,
	uint8_t *pAppKey,
	uint8_t *pNwkKey,
	uint32_t* pDevAddr,
	uint32_t* pNetId,
	uint8_t *pAppSKey,
	uint8_t *pNwkSEncKey,
	uint8_t *pFNwkSIntKey,
	uint8_t *pSNwkSIntKey
	)
{
	uint32_t	uiSUM;
#ifdef MAIN_STANDALONE
#else
	uiSUM = calcCHKSUM(stLORA_USER_CTX.stLoRaWANParams.auiDEV_EUI,8);
	if (uiSUM || stLORA_USER_CTX.stLoRaWANParams.uiABP)
	{
		if (
				(pDevAddr != NULL)
					&&
				(pNetId != NULL)
					&&
				(pAppSKey != NULL)
					&&
				(pNwkSEncKey != NULL)
					&&
				(pFNwkSIntKey != NULL)
					&&
				(pSNwkSIntKey != NULL)
			)
		*pDevAddr = 0;
		*pNetId = 0;
		if (stLORA_USER_CTX.stLoRaWANParams.uiABP)
		{
			*pDevAddr = stLORA_USER_CTX.stLoRaWANParams.auiABP_DEV_ADDRESS[0];
			*pDevAddr <<=8;
			*pDevAddr |= stLORA_USER_CTX.stLoRaWANParams.auiABP_DEV_ADDRESS[1];
			*pDevAddr <<=8;
			*pDevAddr |= stLORA_USER_CTX.stLoRaWANParams.auiABP_DEV_ADDRESS[2];
			*pDevAddr <<=8;
			*pDevAddr |= stLORA_USER_CTX.stLoRaWANParams.auiABP_DEV_ADDRESS[3];

			*pNetId = stLORA_USER_CTX.stLoRaWANParams.auiABP_NWK_ID[0];
			*pNetId <<=8;
			*pNetId |= stLORA_USER_CTX.stLoRaWANParams.auiABP_NWK_ID[1];
			*pNetId <<=8;
			*pNetId |= stLORA_USER_CTX.stLoRaWANParams.auiABP_NWK_ID[2];
			*pNetId <<=8;
			*pNetId |= stLORA_USER_CTX.stLoRaWANParams.auiABP_NWK_ID[3];

			// ABP

			memcpy(pAppSKey		,stLORA_USER_CTX.stLoRaWANParams.auiAPP_KEY__ABP_S_KEY,16);
			memcpy(pNwkSEncKey	,stLORA_USER_CTX.stLoRaWANParams.auiNWK_KEY__ABP_S_KEY,16);
			memcpy(pFNwkSIntKey	,stLORA_USER_CTX.stLoRaWANParams.auiABP_F_NWK_S_INT_KEY,16);
			memcpy(pSNwkSIntKey	,stLORA_USER_CTX.stLoRaWANParams.auiABP_S_NWK_S_INT_KEY,16);
		}
		else
		{

		}

		// OTAA
		memcpy(pDevEui		,stLORA_USER_CTX.stLoRaWANParams.auiDEV_EUI,8);
		memcpy(pJoinEui		,stLORA_USER_CTX.stLoRaWANParams.auiJOIN_EUI,8);
		memcpy(pAppKey		,stLORA_USER_CTX.stLoRaWANParams.auiAPP_KEY__ABP_S_KEY,16);
		memcpy(pNwkKey		,stLORA_USER_CTX.stLoRaWANParams.auiNWK_KEY__ABP_S_KEY,16);


	}
	else
#endif
	{
		// If DevEUI i null then take the hardware default
		HW_GetUniqueId(pDevEui);
	}
}

uint8_t	lorauserConvertError(enum eLoRaMacStatus eLoRaMacStatus);
uint8_t	lorauserConvertError(enum eLoRaMacStatus eLoRaMacStatus)
{

	static const uint8_t aLoRaMacToINTCOM[] =
	{
			LORAWAN_STATUS_OK, 						// LORAMAC_STATUS_OK
			LORAWAN_MAC_STATUS_CERTIFICATION_RUNNING, // LORAMAC_STATUS_BUSY
			LORAWAN_MAC_STATUS_SERVICE_UNKNOWN, 		// LORAMAC_STATUS_SERVICE_UNKNOWN
			LORAWAN_MAC_STATUS_PARAMETER_INVALID, 	// LORAMAC_STATUS_PARAMETER_INVALID
			LORAWAN_MAC_STATUS_FREQUENCY_INVALID, 	// LORAMAC_STATUS_FREQUENCY_INVALID
			LORAWAN_MAC_STATUS_DATARATE_INVALID, 		// LORAMAC_STATUS_DATARATE_INVALID
			LORAWAN_MAC_STATUS_FREQ_AND_DR_INVALID, 	// LORAMAC_STATUS_FREQ_AND_DR_INVALID
			LORAWAN_MAC_STATUS_NO_NETWORK_JOINED, 	// LORAMAC_STATUS_NO_NETWORK_JOINED
			LORAWAN_MAC_STATUS_LENGTH_ERROR, 			// LORAMAC_STATUS_LENGTH_ERROR
//			LORAWAN_MAC_STATUS_DEVICE_OFF, 			// LORAMAC_STATUS_DEVICE_OFF
			LORAWAN_MAC_STATUS_REGION_NOT_SUPPORTED, 	// LORAMAC_STATUS_REGION_NOT_SUPPORTED
			LORAWAN_MAC_STATUS_TX_SYS_BUSY, 	// LORAMAC_STATUS_SKIPPED_APP_DATA
			LORAWAN_MAC_STATUS_DUTY_VIOLATION, 		// LORAMAC_STATUS_DUTYCYCLE_RESTRICTED
			LORAWAN_MAC_STATUS_NO_CHANNEL_FOUND, 		// LORAMAC_STATUS_NO_CHANNEL_FOUND
			LORAWAN_MAC_STATUS_NO_CHANNEL_FOUND	 	// LORAMAC_STATUS_NO_FREE_CHANNEL_FOUND
	};
	uint8_t uiRet;

	uiRet = LORAWAN_STATUS_GENERIC_LORAMAC_FAULT;
	if (eLoRaMacStatus < sizeof(aLoRaMacToINTCOM))
	{
		uiRet = aLoRaMacToINTCOM[eLoRaMacStatus];
	}
	return uiRet;
}

uint8_t lora_userJoin(uint8_t uiBaseDataRate, LoRaMacCurrentTxInfos_t* pLoRaMacCurrentTxInfos )
{
	uint8_t uiRetStatus;
	uiRetStatus = LORAWAN_STATUS_NINIT;
	if (stLORA_USER_CTX.bInitOK )
	{
		LoRaMacStatus_t	eLoRaMacStatus;
		LoRaParam.TxDatarate = uiBaseDataRate;
		eLoRaMacStatus = LORA_Join( pLoRaMacCurrentTxInfos);
		switch (eLoRaMacStatus)
		{
		case LORAMAC_STATUS_OK:
			stLORA_USER_CTX.bEnabledOK 	= true;
			stLORA_USER_CTX.bJoinedOK 	= false;
			break;
		case LORAMAC_STATUS_DUTYCYCLE_RESTRICTED:
		    PRINTF( "JoinTx DutyCycle Restricted of %d ms\n\r", ptimeNextTxDelay ? *ptimeNextTxDelay : -1);

			break;
		default:
			// serve un codificatore di errore!!!
		case LORAMAC_STATUS_REGION_NOT_SUPPORTED:
			break;

		}
		uiRetStatus = lorauserConvertError(eLoRaMacStatus);

	}
	return uiRetStatus;
}





uint8_t lora_user_SendData(unsigned uiPort, bool bConfirmNeeded, unsigned uiSize, const uint8_t* abyBuffer, LoRaMacCurrentTxInfos_t* pLoRaMacCurrentTxInfos ,int* peLoRaMacStatus)
{
	uint8_t	uiRet;

	if (peLoRaMacStatus != NULL)
	{
		*peLoRaMacStatus = 0;
	}

	if (uiSize > LORA_USER_TR_BUFFER_MAX)
	{
		return LORAWAN_STATUS_INVAL;
	}

	uiRet = LORAWAN_STATUS_NINIT;

	if (stLORA_USER_CTX.bInitOK)
	{
		uiRet = LORAWAN_STATUS_NENABLE;
		if (stLORA_USER_CTX.bEnabledOK)
		{
			uiRet = LORAWAN_STATUS_NJOIN;
			if ( LORA_JoinStatus () == LORA_SET)
			{
				uiRet = LORAWAN_STATUS_OK;
				stLORA_USER_CTX.bJoinedOK 	= true;
			}
		}
	}
	if (stLORA_USER_CTX.bJoinedOK)
	{
		enum eLoRaMacStatus	eLoRaMacStatus;

		stLORA_USER_CTX.stTRANSMITION.stTxSlot.Port = uiPort;
		stLORA_USER_CTX.stTRANSMITION.stTxSlot.BuffSize = 0;
		if ((abyBuffer != NULL) && (uiSize > 0))
		{
			memcpy(stLORA_USER_CTX.stTRANSMITION.stTxSlot.Buff,abyBuffer,uiSize);
			stLORA_USER_CTX.stTRANSMITION.stTxSlot.BuffSize = uiSize;
		}

		eLoRaMacStatus = LORA_send( &stLORA_USER_CTX.stTRANSMITION.stTxSlot, bConfirmNeeded ? LORAWAN_CONFIRMED_MSG : LORAWAN_UNCONFIRMED_MSG , pLoRaMacCurrentTxInfos );
		switch (eLoRaMacStatus)
		{
		default:
			break;
		case LORAMAC_STATUS_DUTYCYCLE_RESTRICTED:
		    PRINTF( "NormalTx DutyCycle Restricted of %d ms\n\r", ptimeNextTxDelay ? *ptimeNextTxDelay : -1);
		    break;
		}
		if (peLoRaMacStatus != NULL)
		{
			*peLoRaMacStatus = eLoRaMacStatus;
		}

		uiRet = lorauserConvertError(eLoRaMacStatus);
	}
	return uiRet;
}

