/*
 * lora_hw_scv.h
 *
 *  Created on: 12 mar 2018
 *      Author: daniele_parise
 */

#ifndef SRC_APPLICATION_LORA_LORA_HW_SCV_H_
#define SRC_APPLICATION_LORA_LORA_HW_SCV_H_

uint8_t lora_hw_svc_GetBatteryLevel(void);
uint16_t lora_hw_svc_TemperatureLevel(void);

typedef struct tagLORA_HW_SVC_DEV_INFOS{
	uint16_t	uiTemperature;
	uint8_t		uiBatteryLevel;
	uint8_t		uiSpare;
}LORA_HW_SVC_DEV_INFOS;

extern LORA_HW_SVC_DEV_INFOS	stLORA_HW_SVC_DEV_INFOS;

#endif /* SRC_APPLICATION_LORA_LORA_HW_SCV_H_ */
