/*
 * tag_user.c
 *
 *  Created on: 03 mar 2019
 *      Author: daniele_parise
 */
#include <stdint.h>


#define PRGTAG_MCUTYPE		(0x8801)
#define PRGTAG_TYPE			(0xD181)
#define PRGTAG_VER_MIN		(0x05)
#define PRGTAG_VER_MAJ		(0x0A)
#define PRGTAG_BUILD		(0x0001)


typedef struct tagPRGTAGINFO_EXT{
	uint16_t	uiPLATFORM;
	uint16_t	uiAPPLICATION;
	uint16_t	uiVERSION;
	uint16_t	uiFREE_03;
	uint16_t	uiBUILD;
	uint16_t	uiFREE_05;
	uint16_t	uiFREE_06;
	uint16_t	uiFREE_07;
	uint16_t	uiFREE_08;
	uint16_t	uiFREE_09;
	uint16_t	uiFREE_0A;
	uint16_t	uiFREE_0B;
	uint16_t	uiFREE_0C;
	uint16_t	uiFREE_0D;
	uint16_t	uiCRC32_HI;
	uint16_t	uiCRC32_LO;
}PRGTAGINFO_EXT;

#define PTAGMODEL __attribute__((section(".user_ptag")))

PTAGMODEL const PRGTAGINFO_EXT	stPRGTAG_USER = {
	PRGTAG_MCUTYPE,
	PRGTAG_TYPE,
	(((PRGTAG_VER_MAJ << 8) & 0xFF00) | ((PRGTAG_VER_MIN << 0) & 0x00FF)),
	0xFFFF,
	PRGTAG_BUILD,
	0xFFFF,
	0xFFFF,
	0xFFFF,
	0xFFFF,
	0xFFFF,
	0xFFFF,
	0xFFFF,
	0xFFFF,
	0xFFFF,
	0xFFFF,
	0xFFFF
};
