/*
 * timeSrvCheck.h
 *
 *  Created on: 29 mar 2018
 *      Author: daniele_parise
 */

#ifndef SRC_APPLICATION_TIMESERVER_TIMESRVCHECK_H_
#define SRC_APPLICATION_TIMESERVER_TIMESRVCHECK_H_

void timerServerCheckInit(void);
void timerServerCheck(void);

#endif /* SRC_APPLICATION_TIMESERVER_TIMESRVCHECK_H_ */
