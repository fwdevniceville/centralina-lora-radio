/*
 * timeSrvCheck.c
 *
 *  Created on: 29 mar 2018
 *      Author: daniele_parise
 */
#include <stdbool.h>
#include "timeSrvCheck.h"
#include "hw_rtc.h"
#include "timeServer.h"

static TimerEvent_t timerServerTimer;

void timerServerCheckTimer(  void* context  );
void timerServerCheckTimer(  void* context  )
{

}
void timerServerCheckInit(void)
{
    TimerInit( &timerServerTimer, timerServerCheckTimer );

}

void timerServerCheck(void)
{
	int64_t		ideltaTime;
	uint32_t	uiReference;
	ideltaTime = HW_RTC_GetTimerElapsedTime();

	if (ideltaTime < 0)
	{
		uiReference = 120 * 1000;
	}
	else
	{
		uiReference = HW_RTC_Tick2ms((uint32_t)ideltaTime);
	}
	if (uiReference > 60 * 1000)
	{
	    TimerStop(&timerServerTimer);
	    TimerSetValue( &timerServerTimer,  10);
	    TimerStart(&timerServerTimer);
	}
}

