/*
 * crc16.c
 *
 *  Created on: 27 feb 2018
 *      Author: daniele_parise
 */
#include <stdint.h>
#include "crc16.h"

uint16_t calcCRC16(uint16_t wCONTINUE_FROM, const uint8_t* pbyBuffer, uint16_t wSize)
{
	uint16_t	wRetCRC;
	uint16_t	wScanner;
	uint8_t		byShiftCounter;

	wRetCRC = wCONTINUE_FROM;
	wScanner = 0;

	while (wScanner < wSize)
	{
		wRetCRC = (wRetCRC & 0xFF00) | (((uint8_t)(wRetCRC) ^ pbyBuffer[wScanner]) & 0x00FF);

		byShiftCounter = 0;
		while (byShiftCounter < 8 )
		{
			byShiftCounter++;
			if (wRetCRC & 0x0001)
			{
				wRetCRC >>= 1;
				wRetCRC ^= 0xA001;
			}
			else
			{
				wRetCRC >>= 1;
			}
		}
		wScanner++;
	}

	return wRetCRC;
}
