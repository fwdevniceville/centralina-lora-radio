/*
 * chksum.h
 *
 *  Created on: 26 dic 2018
 *      Author: daniele_parise
 */

#ifndef SRC_APPLICATION_UTILS_CHKSUM_H_
#define SRC_APPLICATION_UTILS_CHKSUM_H_


uint32_t calcCHKSUM(const uint8_t* pbyBuffer, uint32_t uiSize);

#endif /* SRC_APPLICATION_UTILS_CHKSUM_H_ */
