/*
 * chksum.c
 *
 *  Created on: 26 dic 2018
 *      Author: daniele_parise
 */
#include <stdint.h>
#include "chksum.h"

uint32_t calcCHKSUM(const uint8_t* pbyBuffer, uint32_t uiSize)
{
	uint32_t	ulRET;
	ulRET = 0;
	while (uiSize--)
	{
		ulRET+=*(pbyBuffer++);
	}
	return ulRET;
}
