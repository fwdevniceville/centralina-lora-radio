/*
 * crc16.h
 *
 *  Created on: 27 feb 2018
 *      Author: daniele_parise
 */

#ifndef UTILS_CRC16_H_
#define UTILS_CRC16_H_

uint16_t calcCRC16(uint16_t wCONTINUE_FROM, const uint8_t* pbyBuffer, uint16_t wSize);

#endif /* UTILS_CRC16_H_ */
