/*
 * standalone.h
 *
 *  Created on: 13 ago 2018
 *      Author: daniele_parise
 */

#ifndef SRC_APPLICATION_STANDALONE_STANDALONE_H_
#define SRC_APPLICATION_STANDALONE_STANDALONE_H_

void standaloneInit();
void standaloneMain();

#endif /* SRC_APPLICATION_STANDALONE_STANDALONE_H_ */
