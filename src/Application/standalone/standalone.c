/*
 * standalone.c
 *
 *  Created on: 13 ago 2018
 *      Author: daniele_parise
 */

#include "timeServer.h"
#include "lora.h"
#include "../lora/lora_user_def.h"
#include "util_console.h"
#include "hw_gpio.h"

#define USE_B_L072Z()	1
#define USE_B_L072Z_LRWAN1	1

#if (USE_B_L072Z())
#include "b-l072z-lrwan1.h"
#endif

#ifdef USE_B_L072Z_LRWAN1
/*!
 * Timer to handle the application Tx Led to toggle
 */
static TimerEvent_t TxLedTimer;
static void OnTimerLedEvent( void* context );
#endif

typedef struct tagSTANDALONE_CTX{
	bool	bSend;
}STANDALONE_CTX;

STANDALONE_CTX	stSTANDALONE_CTX;

void LORA_TxNeeded ( void );
void LORA_ConfirmClass ( DeviceClass_t Class );
void LoraStartTxTimer(void ) ;
void LoraStartTxButton( void) ;

void HW_GetUniqueId( uint8_t *id );
uint16_t HW_GetTemperatureLevel( void ) ;
uint8_t HW_GetBatteryLevel( void ) ;
uint32_t HW_GetRandomSeed( void );
void LORA_RxData( lora_AppData_t *AppData , const McpsIndication_t *McpsIndication );
void LORA_HasJoined( void );
void LORA_TOnMacMcpsIndicationOthers ( const McpsIndication_t *McpsIndication );

void lora_standalone_GetJoinData(
	uint8_t *DevEui,
	uint8_t *JoinEui,
	uint8_t *AppKey,
	uint8_t *NwkKey,
	uint32_t* DevAddr,
	uint32_t* NetId,
	uint8_t *AppSKey,
	uint8_t *NwkSEncKey,
	uint8_t *FNwkSIntKey,
	uint8_t *SNwkSIntKey
	);

void lora_user_internal_OnRxData( lora_AppData_t *AppData, const McpsIndication_t *McpsIndication );
void lora_user_internal_OnMacMcpsIndicationOthers ( const McpsIndication_t *mcpsIndication );
void lora_user_internal_OnHasJoined( void );
void lora_user_internal_OnMacProcessNotify ( void );


static LoRaMainCallback_t LoRaMainCallbacks = { HW_GetBatteryLevel,
                                                HW_GetTemperatureLevel,
												lora_standalone_GetJoinData,
                                                HW_GetRandomSeed,
												lora_user_internal_OnRxData, //LORA_RxData,
												lora_user_internal_OnHasJoined, //LORA_HasJoined,
                                                LORA_ConfirmClass,
                                                LORA_TxNeeded,
												lora_user_internal_OnMacProcessNotify,
												lora_user_internal_OnMacMcpsIndicationOthers //LORA_TOnMacMcpsIndicationOthers
												};

static  LoRaParam_t LoRaParamInit= {LORAWAN_ADR_STATE,
									DR_0,//LORAWAN_DEFAULT_DATA_RATE,
                                    LORAWAN_PUBLIC_NETWORK};


void lora_standalone_GetJoinData(
	uint8_t *pDevEui,
	uint8_t *pJoinEui,
	uint8_t *pAppKey,
	uint8_t *pNwkKey,
	uint32_t* pDevAddr,
	uint32_t* pNetId,
	uint8_t *pAppSKey,
	uint8_t *pNwkSEncKey,
	uint8_t *pFNwkSIntKey,
	uint8_t *pSNwkSIntKey
	)
{
	uint32_t	uiSUM;
#ifdef MAIN_STANDALONE
#else
	uiSUM = calcCHKSUM(stLORA_USER_CTX.stLoRaWANParams.auiDEV_EUI,8);
	if (uiSUM || stLORA_USER_CTX.stLoRaWANParams.uiABP)
	{
		if (
				(pDevAddr != NULL)
					&&
				(pNetId != NULL)
					&&
				(pAppSKey != NULL)
					&&
				(pNwkSEncKey != NULL)
					&&
				(pFNwkSIntKey != NULL)
					&&
				(pSNwkSIntKey != NULL)
			)
		*pDevAddr = 0;
		*pNetId = 0;
		if (stLORA_USER_CTX.stLoRaWANParams.uiABP)
		{
			*pDevAddr = stLORA_USER_CTX.stLoRaWANParams.auiABP_DEV_ADDRESS[0];
			*pDevAddr <<=8;
			*pDevAddr |= stLORA_USER_CTX.stLoRaWANParams.auiABP_DEV_ADDRESS[1];
			*pDevAddr <<=8;
			*pDevAddr |= stLORA_USER_CTX.stLoRaWANParams.auiABP_DEV_ADDRESS[2];
			*pDevAddr <<=8;
			*pDevAddr |= stLORA_USER_CTX.stLoRaWANParams.auiABP_DEV_ADDRESS[3];

			*pNetId = stLORA_USER_CTX.stLoRaWANParams.auiABP_NWK_ID[0];
			*pNetId <<=8;
			*pNetId |= stLORA_USER_CTX.stLoRaWANParams.auiABP_NWK_ID[1];
			*pNetId <<=8;
			*pNetId |= stLORA_USER_CTX.stLoRaWANParams.auiABP_NWK_ID[2];
			*pNetId <<=8;
			*pNetId |= stLORA_USER_CTX.stLoRaWANParams.auiABP_NWK_ID[3];

			// ABP

			memcpy(pAppSKey		,stLORA_USER_CTX.stLoRaWANParams.auiAPP_KEY__ABP_S_KEY,16);
			memcpy(pNwkSEncKey	,stLORA_USER_CTX.stLoRaWANParams.auiNWK_KEY__ABP_S_KEY,16);
			memcpy(pFNwkSIntKey	,stLORA_USER_CTX.stLoRaWANParams.auiABP_F_NWK_S_INT_KEY,16);
			memcpy(pSNwkSIntKey	,stLORA_USER_CTX.stLoRaWANParams.auiABP_S_NWK_S_INT_KEY,16);
		}
		else
		{

		}

		// OTAA
		memcpy(pDevEui		,stLORA_USER_CTX.stLoRaWANParams.auiDEV_EUI,8);
//		memcpy(pJoinEui		,stLORA_USER_CTX.stLoRaWANParams.auiJOIN_EUI,8);
//		memcpy(pAppKey		,stLORA_USER_CTX.stLoRaWANParams.auiAPP_KEY__ABP_S_KEY,16);
//		memcpy(pNwkKey		,stLORA_USER_CTX.stLoRaWANParams.auiNWK_KEY__ABP_S_KEY,16);


	}
	else
#endif
	{
		// If DevEUI i null then take the hardware default
		HW_GetUniqueId(pDevEui);
	}
}

void standaloneInit(void)
{
#if (USE_B_L072Z())
//    BSP_LED_Init( LED1 );
//    BSP_LED_Init( LED2 );
    BSP_LED_Init( LED3 );
    BSP_LED_Init( LED4 );
#endif

	  LORA_Init( &LoRaMainCallbacks, &LoRaParamInit);

	  LORA_Join(NULL);

#if (USE_B_L072Z())
	  LoraStartTxTimer( ) ;
	  LoraStartTxButton( ) ;
#ifdef USE_B_L072Z_LRWAN1
	  TimerInit( &TxLedTimer, OnTimerLedEvent );
#endif

#endif

}
void standaloneSend( void );

void standaloneMain(void)
{
	  if (stSTANDALONE_CTX.bSend)
	  {
		  stSTANDALONE_CTX.bSend =false;
		  standaloneSend( );
	  }

}


TimerEvent_t TxTimer;
int	iConfirmedMessages = 0;
uint8_t AppLedStateOn = RESET;
#define LORAWAN_APP_DATA_BUFF_SIZE                           64
/*!
 * User application data
 */
static uint8_t AppDataBuff[LORAWAN_APP_DATA_BUFF_SIZE];
lora_AppData_t AppData={ AppDataBuff,  0 ,0 };

void LORA_HasJoined( void )
{
#if( OVER_THE_AIR_ACTIVATION != 0 )
  PRINTF("JOINED\n\r");
#endif
  LORA_RequestClass( LORAWAN_DEFAULT_CLASS );
}

void LORA_RxData( lora_AppData_t *AppData , const McpsIndication_t *McpsIndication )
{
  /* USER CODE BEGIN 4 */
  PRINTF("PACKET RECEIVED ON PORT %d\n\r", AppData->Port);

  switch (AppData->Port)
  {
    case 3:
    /*this port switches the class*/
    if( AppData->BuffSize == 1 )
    {
      switch (  AppData->Buff[0] )
      {
        case 0:
        {
          LORA_RequestClass(CLASS_A);
          break;
        }
        case 1:
        {
          LORA_RequestClass(CLASS_B);
          break;
        }
        case 2:
        {
          LORA_RequestClass(CLASS_C);
          break;
        }
        default:
          break;
      }
    }
    break;
    case LORAWAN_APP_PORT:
    if( AppData->BuffSize == 1 )
    {
      AppLedStateOn = AppData->Buff[0] & 0x01;
      if ( AppLedStateOn == RESET )
      {
        PRINTF("LED OFF\n\r");
//        LED_Off( LED_BLUE ) ;
      }
      else
      {
        PRINTF("LED ON\n\r");
//        LED_On( LED_BLUE ) ;
      }
    }
    break;
  case LPP_APP_PORT:
  {
    AppLedStateOn= (AppData->Buff[2] == 100) ?  0x01 : 0x00;
    if ( AppLedStateOn == RESET )
    {
      PRINTF("LED OFF\n\r");
//      LED_Off( LED_BLUE ) ;

    }
    else
    {
      PRINTF("LED ON\n\r");
//      LED_On( LED_BLUE ) ;
    }
    break;
  }
  default:
    break;
  }
  /* USER CODE END 4 */
}


typedef struct sensor_t{
	  uint16_t pressure;
	  int16_t temperature;
	  uint16_t humidity;
}sensor_t;

LoRaMacCurrentTxInfos_t stLoRaMacCurrentTxInfos;

void standaloneSend( void )
{
  /* USER CODE BEGIN 3 */
  uint16_t pressure = 0;
  int16_t temperature = 0;
  uint16_t humidity = 0;
  uint8_t batteryLevel;
  sensor_t sensor_data;
  LoraConfirm_t	bConfirmed;
  LoRaMacStatus_t eRet;

  if ( LORA_JoinStatus () != LORA_SET)
  {
    /*Not joined, try again later*/
    LORA_Join(&stLoRaMacCurrentTxInfos);
    return;
  }

  TVL1(PRINTF("SEND REQUEST\n\r");)
#ifndef CAYENNE_LPP
  int32_t latitude, longitude = 0;
  uint16_t altitudeGps = 0;
#endif


//  BSP_sensor_Read( &sensor_data );

#ifdef CAYENNE_LPP
  uint8_t cchannel=0;
  temperature = ( int16_t )( sensor_data.temperature * 10 );     /* in �C * 10 */
  pressure    = ( uint16_t )( sensor_data.pressure * 100 / 10 );  /* in hPa / 10 */
  humidity    = ( uint16_t )( sensor_data.humidity * 2 );        /* in %*2     */
  uint32_t i = 0;

  batteryLevel = HW_GetBatteryLevel( );                     /* 1 (very low) to 254 (fully charged) */

  AppData.Port = LPP_APP_PORT;

  AppData.Buff[i++] = cchannel++;
  AppData.Buff[i++] = LPP_DATATYPE_BAROMETER;
  AppData.Buff[i++] = ( pressure >> 8 ) & 0xFF;
  AppData.Buff[i++] = pressure & 0xFF;
  AppData.Buff[i++] = cchannel++;
  AppData.Buff[i++] = LPP_DATATYPE_TEMPERATURE;
  AppData.Buff[i++] = ( temperature >> 8 ) & 0xFF;
  AppData.Buff[i++] = temperature & 0xFF;
  AppData.Buff[i++] = cchannel++;
  AppData.Buff[i++] = LPP_DATATYPE_HUMIDITY;
  AppData.Buff[i++] = humidity & 0xFF;
#if defined( REGION_US915 ) || defined( REGION_US915_HYBRID ) || defined ( REGION_AU915 )
  /* The maximum payload size does not allow to send more data for lowest DRs */
#else
  AppData.Buff[i++] = cchannel++;
  AppData.Buff[i++] = LPP_DATATYPE_DIGITAL_INPUT;
  AppData.Buff[i++] = batteryLevel*100/254;
  AppData.Buff[i++] = cchannel++;
  AppData.Buff[i++] = LPP_DATATYPE_DIGITAL_OUTPUT;
  AppData.Buff[i++] = AppLedStateOn;
#endif  /* REGION_XX915 */
#else  /* not CAYENNE_LPP */

  temperature = ( int16_t )( sensor_data.temperature * 100 );     /* in �C * 100 */
  pressure    = ( uint16_t )( sensor_data.pressure * 100 / 10 );  /* in hPa / 10 */
  humidity    = ( uint16_t )( sensor_data.humidity * 10 );        /* in %*10     */
  latitude = sensor_data.latitude;
  longitude= sensor_data.longitude;
  uint32_t i = 0;

  batteryLevel = HW_GetBatteryLevel( );                     /* 1 (very low) to 254 (fully charged) */

  AppData.Port = LORAWAN_APP_PORT;

#if defined( REGION_US915 ) || defined( REGION_US915_HYBRID ) || defined ( REGION_AU915 )
  AppData.Buff[i++] = AppLedStateOn;
  AppData.Buff[i++] = ( pressure >> 8 ) & 0xFF;
  AppData.Buff[i++] = pressure & 0xFF;
  AppData.Buff[i++] = ( temperature >> 8 ) & 0xFF;
  AppData.Buff[i++] = temperature & 0xFF;
  AppData.Buff[i++] = ( humidity >> 8 ) & 0xFF;
  AppData.Buff[i++] = humidity & 0xFF;
  AppData.Buff[i++] = batteryLevel;
  AppData.Buff[i++] = 0;
  AppData.Buff[i++] = 0;
  AppData.Buff[i++] = 0;
#else  /* not REGION_XX915 */
  AppData.Buff[i++] = AppLedStateOn;
  AppData.Buff[i++] = ( pressure >> 8 ) & 0xFF;
  AppData.Buff[i++] = pressure & 0xFF;
  AppData.Buff[i++] = ( temperature >> 8 ) & 0xFF;
  AppData.Buff[i++] = temperature & 0xFF;
  AppData.Buff[i++] = ( humidity >> 8 ) & 0xFF;
  AppData.Buff[i++] = humidity & 0xFF;
  AppData.Buff[i++] = batteryLevel;
  AppData.Buff[i++] = ( latitude >> 16 ) & 0xFF;
  AppData.Buff[i++] = ( latitude >> 8 ) & 0xFF;
  AppData.Buff[i++] = latitude & 0xFF;
  AppData.Buff[i++] = ( longitude >> 16 ) & 0xFF;
  AppData.Buff[i++] = ( longitude >> 8 ) & 0xFF;
  AppData.Buff[i++] = longitude & 0xFF;
  AppData.Buff[i++] = ( altitudeGps >> 8 ) & 0xFF;
  AppData.Buff[i++] = altitudeGps & 0xFF;
#endif  /* REGION_XX915 */
#endif  /* CAYENNE_LPP */
  AppData.BuffSize = i;


  bConfirmed = LORAWAN_UNCONFIRMED_MSG;
  if (iConfirmedMessages > 0)
  {
	  bConfirmed = LORAWAN_CONFIRMED_MSG;
  }
  eRet = LORA_send( &AppData, bConfirmed,&stLoRaMacCurrentTxInfos);
  if (eRet == LORAMAC_STATUS_OK)

  {
	  if (iConfirmedMessages > 0)
	  {
		  iConfirmedMessages--;
	  }
  }

#ifdef USE_B_L072Z_LRWAN1
  TimerSetValue(  &TxLedTimer, 200);

  if (eRet == LORAMAC_STATUS_OK)

  {
	  LED_On( LED_BLUE ) ;
  }
  else
  {
	  LED_On( LED_RED2 ) ;
  }

  TimerStart( &TxLedTimer );
#endif

  /* USER CODE END 3 */
}

void standalonePostSend(void *);
void standalonePostSend(void * pctx)
{
	  stSTANDALONE_CTX.bSend = true;

}


void OnTxTimerEvent( void * pctx)
{
  /*Wait for next tx slot*/
  TimerStart( &TxTimer);
  /*Send*/
  standalonePostSend(NULL);
}

void LoraStartTxTimer(void)
{
    /* send everytime timer elapses */
    TimerInit( &TxTimer, OnTxTimerEvent );
    TimerSetValue( &TxTimer,  APP_TX_DUTYCYCLE);
    OnTxTimerEvent(NULL);
}

#ifdef USE_B_L072Z_LRWAN1
static void OnTimerLedEvent( void* context )
{
	  LED_Off( LED_BLUE ) ;
	  LED_Off( LED_RED2 ) ;
}
#endif

void LoraStartTxButton(void)
{
    /* send everytime button is pushed */
    GPIO_InitTypeDef initStruct={0};

    initStruct.Mode =GPIO_MODE_IT_RISING;
    initStruct.Pull = GPIO_PULLUP;
    initStruct.Speed = GPIO_SPEED_HIGH;

    HW_GPIO_Init( USER_BUTTON_GPIO_PORT, USER_BUTTON_PIN, &initStruct );
    HW_GPIO_SetIrq( USER_BUTTON_GPIO_PORT, USER_BUTTON_PIN, 0, standalonePostSend );
}

void LORA_ConfirmClass ( DeviceClass_t Class )
{
  PRINTF("switch to class %c done\n\r","ABC"[Class] );

  /*Optionnal*/
  /*informs the server that switch has occurred ASAP*/
  AppData.BuffSize = 0;
  AppData.Port = LORAWAN_APP_PORT;

  LORA_send( &AppData, LORAWAN_UNCONFIRMED_MSG,NULL);
}

void LORA_TxNeeded ( void )
{
  AppData.BuffSize = 0;
  AppData.Port = LORAWAN_APP_PORT;

  LORA_send( &AppData, LORAWAN_UNCONFIRMED_MSG,NULL);
}

void LORA_TOnMacMcpsIndicationOthers ( const McpsIndication_t *mcpsIndication )
{
	PRINTF("MAC_INDICATION OTH (seqRx= %d) FramePending = %d , rssi = %d, snr = %d, sts= %d\n\r",
			mcpsIndication->DownLinkCounter,
			mcpsIndication->FramePending,
			mcpsIndication->Rssi,
			mcpsIndication->Snr,
			mcpsIndication->Status
			);
/*
	LoraUserRxInfos	stLoraUserRxInfos;
	stLoraUserRxInfos.uiDownLinkCounter = mcpsIndication->DownLinkCounter;
	stLoraUserRxInfos.bFramePending 	= mcpsIndication->FramePending;
	stLoraUserRxInfos.Rssi 				= mcpsIndication->Rssi;
	stLoraUserRxInfos.Snr 				= mcpsIndication->Snr;

	lora_user_OnRxData(&stLoraUserRxInfos,0,0,NULL);
*/
//	FUNZIONA????
}
