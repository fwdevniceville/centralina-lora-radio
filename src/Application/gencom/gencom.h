/*
 * gencom.h
 *
 *  Created on: 18 dic 2018
 *      Author: daniele_parise
 */

#ifndef SRC_APPLICATION_GENCOM_GENCOM_H_
#define SRC_APPLICATION_GENCOM_GENCOM_H_

typedef enum etagEGENCOM_SEND_REASON{
	GENCOM_SEND_MESSAGE,
	GENCOM_SEND_ANSWER,
	GENCOM_SEND_ROUTED_ANSWER
}EGENCOM_SEND_REASON;

struct tagGENCOM_CTX* pGENCOM_CTX;
typedef	bool (*gencom_rx_callback_t)(struct tagGENCOM_CTX* pGENCOM_CTX,uint8_t uiCmd, unsigned uiSize, unsigned uiProgr,const uint8_t* abyBuffer);
typedef	bool (*gencom_txc_callback_t)(struct tagGENCOM_CTX* pGENCOM_CTX,EGENCOM_SEND_REASON eReason,uint8_t uiCmd, unsigned uiSize, unsigned uiProgr,const uint8_t* abyBuffer);

#define GENCOM_BUFF_DEPT	(256 + 4 + 2)
typedef struct tagGENCOM_CTX{
	struct {
		uint8_t		abyBUFF[GENCOM_BUFF_DEPT];
		uint16_t	uiCurrent;
		uint16_t	uiTotals;
		EGENCOM_SEND_REASON eReason;
	}stTX;
	struct {
		uint8_t		abyBUFF[GENCOM_BUFF_DEPT];
		uint16_t	uiCurrent;
		uint16_t	uiTotalsExpected;
		uint16_t	uiTotalsReals;
		uint16_t	uiPayloadSize;
	}stRX;
	struct {
		unsigned uiRX_HW_OVER;
		unsigned uiRX_HW_OVER_IN_MSG;
		unsigned uiRX_HW_FRAM;
		unsigned uiRX_HW_NOISE;

		unsigned uiRX_CRC;
		unsigned uiRX_BREAK_IN_SIZE;
		unsigned uiRX_BREAK_IN_MIN_SIZE;
	}stERRORS;

	void 					*huart;
	gencom_rx_callback_t	rxCallback;
	gencom_txc_callback_t	txcCallback;

	unsigned uiRXCicles;

}GENCOM_CTX;


void gencom_Init(GENCOM_CTX* pGENCOM_CTX,void *huart,gencom_rx_callback_t rxCallback,gencom_txc_callback_t txcCallback);
void gencom_DeInit(GENCOM_CTX* pGENCOM_CTX);
void gencom_process_lpm_rx(GENCOM_CTX* pGENCOM_CTX);
bool gencom_IsSendFree(GENCOM_CTX* pGENCOM_CTX);


int gencom_Send(GENCOM_CTX* pGENCOM_CTX,EGENCOM_SEND_REASON eReason,unsigned uiCmd,unsigned uiSize, unsigned uiProgr,const uint8_t* pSendBuffer);
void gencom_IRQHandler( GENCOM_CTX* pGENCOM_CTX );
void gencom_process_lpm_rx(GENCOM_CTX* pGENCOM_CTX);
void gencom_it_rx_frame(GENCOM_CTX* pGENCOM_CTX,uint8_t data_rx);
// to implement
void gencom_hw_lpm_stop(GENCOM_CTX* pGENCOM_CTX,bool bTX,bool bEnable);
void gencom_hwSignalStopMode(void);
void gencom_hwSignalResponse(void);


#endif /* SRC_APPLICATION_GENCOM_GENCOM_H_ */
