/*
 * gencom.c
 *
 *  Created on: 18 dic 2018
 *      Author: daniele_parise
 */


#include <stdbool.h>
#include <stdint.h>
#include "gencom.h"
#include <string.h>
#include "../Utils/crc16.h"
#include "../hwbrd/hw_bridge.h"


void gencom_Init(GENCOM_CTX* pGENCOM_CTX,void *huart,gencom_rx_callback_t rxCallback,gencom_txc_callback_t txcCallback)
{
	pGENCOM_CTX->huart			= (UART_HandleTypeDef *)huart;
	pGENCOM_CTX->rxCallback		= rxCallback;
	pGENCOM_CTX->txcCallback	= txcCallback;

}

void gencom_DeInit(GENCOM_CTX* pGENCOM_CTX)
{
	pGENCOM_CTX->rxCallback	= NULL;

}

bool gencom_IsSendFree(GENCOM_CTX* pGENCOM_CTX)
{
	if (pGENCOM_CTX->stTX.uiTotals > 0)
	{
		return false;
	}
	return true;
}

int gencom_Send(GENCOM_CTX* pGENCOM_CTX,EGENCOM_SEND_REASON eReason,unsigned uiCmd,unsigned uiSize, unsigned uiProgr,const uint8_t* pSendBuffer)
{
	int iRet;
	iRet = 0;
	if (pGENCOM_CTX->stTX.uiTotals == 0)
	{
		unsigned uiBufferSize;
		UART_HandleTypeDef *huart;
		huart = pGENCOM_CTX->huart;


		uiBufferSize = 1 + 1 + 1 + 2 + uiSize;
		if (uiBufferSize < GENCOM_BUFF_DEPT)
		{
			uint16_t uiCRC;
			pGENCOM_CTX->stTX.eReason	 = eReason;
			pGENCOM_CTX->stTX.abyBUFF[0] = uiSize;
			pGENCOM_CTX->stTX.abyBUFF[1] = uiCmd;
			pGENCOM_CTX->stTX.abyBUFF[2] = uiProgr;
			memcpy(&pGENCOM_CTX->stTX.abyBUFF[3],pSendBuffer,uiSize);
			uiCRC = calcCRC16(0xFFFF,&pGENCOM_CTX->stTX.abyBUFF[0],uiSize + 3);

			pGENCOM_CTX->stTX.abyBUFF[uiSize + 3] = (uint8_t)uiCRC;
			pGENCOM_CTX->stTX.abyBUFF[uiSize + 4] = (uint8_t)(uiCRC>>8);

			pGENCOM_CTX->stTX.uiTotals 	= uiBufferSize;
			pGENCOM_CTX->stTX.uiCurrent = 0;

			gencom_hw_lpm_stop(pGENCOM_CTX,true,false);


			SET_BIT(huart->Instance->CR1, USART_CR1_TE); // buttiamo fuori un idle frame
			SET_BIT(huart->Instance->CR1, USART_CR1_TXEIE);
			CLEAR_BIT(huart->Instance->CR1, USART_CR1_TCIE);
			iRet = uiBufferSize;
		}

	}
	return iRet;
}


void gencom_it_rx_frame(GENCOM_CTX* pGENCOM_CTX,uint8_t data_rx)
{
	UART_HandleTypeDef *huart;
	huart = pGENCOM_CTX->huart;
	gencom_hw_lpm_stop(pGENCOM_CTX,false,false);
	__HAL_UART_CLEAR_IT(huart, UART_CLEAR_IDLEF);
	SET_BIT(huart->Instance->CR1, USART_CR1_IDLEIE);
	pGENCOM_CTX->uiRXCicles++;
	if (pGENCOM_CTX->stRX.uiTotalsReals > 0)
	{
		CLEAR_BIT(huart->Instance->CR1, USART_CR1_RXNEIE);
	}
	else
	{
		if (
				(pGENCOM_CTX->stRX.uiCurrent <= 3)
				||
				(pGENCOM_CTX->stRX.uiCurrent < pGENCOM_CTX->stRX.uiTotalsExpected)
			)
		{
			pGENCOM_CTX->stRX.abyBUFF[pGENCOM_CTX->stRX.uiCurrent] = (uint8_t)data_rx;

			if (pGENCOM_CTX->stRX.uiCurrent == 3)
			{
				pGENCOM_CTX->stRX.uiPayloadSize		= pGENCOM_CTX->stRX.abyBUFF[0];
				pGENCOM_CTX->stRX.uiTotalsExpected	=
						1 // sizer field
						+
						1 // size command
						+
						1 // progr command
						+
						pGENCOM_CTX->stRX.uiPayloadSize // Payload size
						+
						2; // crc16 size

				if (pGENCOM_CTX->stRX.uiTotalsExpected > GENCOM_BUFF_DEPT)
				{
					pGENCOM_CTX->stRX.uiTotalsExpected = 1;
				}
			}
			pGENCOM_CTX->stRX.uiCurrent++;
		}
	}
}




void gencom_IRQHandler( GENCOM_CTX* pGENCOM_CTX )
{

	UART_HandleTypeDef *huart;
	huart = pGENCOM_CTX->huart;
	uint32_t isrflags   = READ_REG(huart->Instance->ISR);
	uint32_t cr1its     = READ_REG(huart->Instance->CR1);
	uint32_t cr3its		= READ_REG(huart->Instance->CR3);
	uint32_t errorflags;
	uint16_t	data_rx	= READ_REG(huart->Instance->RDR);
	gencom_hwSignalResponse();

	//If no error occurs
	errorflags = (isrflags & (uint32_t)(USART_ISR_PE | USART_ISR_FE | USART_ISR_ORE | USART_ISR_NE));
	if (errorflags)
	{
		if (errorflags & USART_ISR_FE)
		{
			pGENCOM_CTX->stERRORS.uiRX_HW_FRAM++;
		}
		if (errorflags & USART_ISR_ORE)
		{
			pGENCOM_CTX->stERRORS.uiRX_HW_OVER++;
			if (pGENCOM_CTX->stRX.uiCurrent > 0)
			{
				pGENCOM_CTX->stERRORS.uiRX_HW_OVER_IN_MSG++;

			}
		}
		if (errorflags & USART_ISR_NE)
		{
			pGENCOM_CTX->stERRORS.uiRX_HW_NOISE++;
		}



//    	intcom_IRQHandler_rx_error();
	      __HAL_UART_CLEAR_IT(huart, UART_CLEAR_PEF);/*!< Parity Error Clear Flag           */
	      __HAL_UART_CLEAR_IT(huart, UART_CLEAR_FEF);/*!< Framing Error Clear Flag          */
	      __HAL_UART_CLEAR_IT(huart, UART_CLEAR_NEF);/*!< Noise detected Clear Flag         */
	      __HAL_UART_CLEAR_IT(huart, UART_CLEAR_OREF);/*!< Overrun Error Clear Flag          */
		SET_BIT(huart->Instance->CR1, USART_CR1_IDLEIE);
		CLEAR_BIT(huart->Instance->CR1, USART_CR1_RXNEIE);
	}
	else
	{
	    if((isrflags & USART_ISR_RXNE) && (cr1its & USART_CR1_RXNEIE))
	    {
	    	gencom_it_rx_frame(pGENCOM_CTX,data_rx);
	    }
	    else
	    if((isrflags & USART_ISR_IDLE) && (cr1its & USART_CR1_IDLEIE))
	    {
	    	if (pGENCOM_CTX->stRX.uiTotalsReals == 0)
	    	{
	    		if (pGENCOM_CTX->stRX.uiTotalsExpected >= (1 + 1 + 1 + 2))
	    		{
	    			if (pGENCOM_CTX->stRX.uiTotalsExpected == pGENCOM_CTX->stRX.uiCurrent)
	    			{
						uint16_t	uiCRC;
						uint16_t	uiCRC_RCV;
						uiCRC = calcCRC16(0xFFFF,&pGENCOM_CTX->stRX.abyBUFF[0],pGENCOM_CTX->stRX.uiTotalsExpected - sizeof(uint16_t));
						uiCRC_RCV = pGENCOM_CTX->stRX.abyBUFF[pGENCOM_CTX->stRX.uiTotalsExpected - 2];
						uiCRC_RCV |= (uint16_t)pGENCOM_CTX->stRX.abyBUFF[pGENCOM_CTX->stRX.uiTotalsExpected - 1] << 8;
						if (uiCRC == uiCRC_RCV)
						{
				    		pGENCOM_CTX->stRX.uiTotalsReals = pGENCOM_CTX->stRX.uiTotalsExpected;
						}
						else
						{
							pGENCOM_CTX->stERRORS.uiRX_CRC++;
						}
	    			}
	    			else
	    			{
						pGENCOM_CTX->stERRORS.uiRX_BREAK_IN_SIZE++;
	    			}
	    		}
	    		else
	    		{
					pGENCOM_CTX->stERRORS.uiRX_BREAK_IN_MIN_SIZE++;
	    		}
	    	}
	    	if (pGENCOM_CTX->stRX.uiTotalsReals > 0)
	    	{
	    		if (pGENCOM_CTX->rxCallback)
	    		{
	    			pGENCOM_CTX->rxCallback(
	    					pGENCOM_CTX,
	    					pGENCOM_CTX->stRX.abyBUFF[1],
							pGENCOM_CTX->stRX.uiPayloadSize,
	    					pGENCOM_CTX->stRX.abyBUFF[2],
	    					&pGENCOM_CTX->stRX.abyBUFF[3]);

	    			pGENCOM_CTX->stRX.uiTotalsReals = 0;
	    		}
	    	}
	    	if (pGENCOM_CTX->stRX.uiTotalsReals == 0)
	    	{
    			pGENCOM_CTX->stRX.uiCurrent 		= 0;
    			pGENCOM_CTX->stRX.uiTotalsExpected 	= 0;
	    	}
			__HAL_UART_CLEAR_IT(huart, UART_CLEAR_IDLEF);
			CLEAR_BIT(huart->Instance->CR1, USART_CR1_IDLEIE);
			SET_BIT(huart->Instance->CR1, USART_CR1_RXNEIE);

			gencom_hw_lpm_stop(pGENCOM_CTX ,false,true);

	    }
	}

	if((isrflags & USART_ISR_WUF) && (cr3its & USART_CR3_WUFIE))
	{
		__HAL_UART_CLEAR_IT(huart, UART_CLEAR_WUF);
	    // Set the UART state ready to be able to start again the process
	}

	// UART in mode Transmitter
	if(((isrflags & USART_ISR_TXE) != RESET) && ((cr1its & USART_CR1_TXEIE) != RESET))
	{
		WRITE_REG(huart->Instance->TDR,pGENCOM_CTX->stTX.abyBUFF[pGENCOM_CTX->stTX.uiCurrent]);
		pGENCOM_CTX->stTX.uiCurrent++;
		if (pGENCOM_CTX->stTX.uiCurrent >= pGENCOM_CTX->stTX.uiTotals)
		{
			SET_BIT(huart->Instance->CR1, USART_CR1_TCIE);
			CLEAR_BIT(huart->Instance->CR1, USART_CR1_TXEIE);
		}
	}
	// UART in mode Transmitter (transmission end)
	if(((isrflags & USART_ISR_TC) != RESET) && ((cr1its & USART_CR1_TCIE) != RESET))
	{
		pGENCOM_CTX->stTX.uiTotals = 0;
		CLEAR_BIT(huart->Instance->CR1, USART_CR1_TXEIE);
		CLEAR_BIT(huart->Instance->CR1, USART_CR1_TCIE);
		CLEAR_BIT(huart->Instance->CR1, USART_CR1_TE);
		if (pGENCOM_CTX->txcCallback)
		{
			uint16_t	uiSize;
			uiSize = pGENCOM_CTX->stTX.abyBUFF[0];
			pGENCOM_CTX->txcCallback(pGENCOM_CTX,pGENCOM_CTX->stTX.eReason,pGENCOM_CTX->stTX.abyBUFF[1],uiSize,pGENCOM_CTX->stTX.abyBUFF[2],&pGENCOM_CTX->stTX.abyBUFF[3]);
		}
		gencom_hw_lpm_stop(pGENCOM_CTX,true,true);


	}
}

void gencom_process_lpm_rx(GENCOM_CTX* pGENCOM_CTX)
{
	UART_HandleTypeDef *huart;
	huart = pGENCOM_CTX->huart;
	uint32_t	isrflags   = READ_REG(huart->Instance->ISR);
	uint32_t	cr1its     = READ_REG(huart->Instance->CR1);
	uint16_t	data_rx	= READ_REG(huart->Instance->RDR);
    if((isrflags & USART_ISR_RXNE) && (cr1its & USART_CR1_RXNEIE))
    {
    	gencom_hwSignalResponse();
    	gencom_it_rx_frame(pGENCOM_CTX,data_rx);
    }
}

