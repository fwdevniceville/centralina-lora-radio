/*
 * gencomhl.c
 *
 *  Created on: 18 mar 2018
 *      Author: daniele_parise
 */

#include <stdbool.h>
#include <stdint.h>
#include "gencom.h"
#include "gencomhl.h"
#include <string.h>
#include "hal_inc.h"

//GENCOMHL_CTX	stGENCOMHL_CTX;
void gencomhlInit(GENCOMHL_CTX* pGENCOMHL_CTX,GENCOM_CTX *pGENCOM_CTX,gencomhl_recv_callback_t pfunc_recv,gencomhl_send_callback_t pfunct_sent, void* pctx_clbk,GENCOMHL_REQUEST_SENDER_CTX* pSENDER_CTX)
{
	pGENCOMHL_CTX->bACTIVE				= true;
	pGENCOMHL_CTX->stCLBK.pfunc_recv	= pfunc_recv;
	pGENCOMHL_CTX->stCLBK.pfunct_sent	= pfunct_sent;
	pGENCOMHL_CTX->stCLBK.pctx_clbk		= pctx_clbk;

	pGENCOMHL_CTX->pGENCOM_CTX					= pGENCOM_CTX;
	pGENCOMHL_CTX->pSENDER_SCHEDULER_CTX		= pSENDER_CTX;

}

void gencomhlFree(GENCOMHL_CTX* pGENCOMHL_CTX)
{
	pGENCOMHL_CTX->bACTIVE				= false;
}

int gencomhlExecComunicationSlot(GENCOMHL_CTX* pGENCOMHL_CTX,int deltaT_ms,unsigned uiEventMask,bool* pbSytemRunning)
{
	unsigned	uiSystemMustRun;
	int	iNextDeltaT_ms;
	int32_t		iRemainigTimeSND;
	int32_t		iRemainigTimeRCV;
	iRemainigTimeSND = -1;
	iRemainigTimeRCV = -1;
	uiSystemMustRun = 0x11;
	iNextDeltaT_ms = -1;


	switch (pGENCOMHL_CTX->stSEND.eSTATUS)
	{
	case GENCOMHL_SEND_FREE:
		uiSystemMustRun &= ~0x01;
		break;
	case GENCOMHL_SEND_POST:
		{
			if (gencom_IsSendFree(pGENCOMHL_CTX->pGENCOM_CTX))
			{
				EGENCOM_SEND_REASON eSendReason;

				eSendReason = GENCOM_SEND_MESSAGE;
				if (pGENCOMHL_CTX->stSEND.stREQUEST.uiCODE & GENCOMHL_CMD_ANSWER_MASK)
				{
					eSendReason = GENCOM_SEND_ROUTED_ANSWER;
				}
				gencom_Send(
						pGENCOMHL_CTX->pGENCOM_CTX,
						eSendReason,
						pGENCOMHL_CTX->stSEND.stREQUEST.uiCODE,
						pGENCOMHL_CTX->stSEND.stREQUEST.uiSIZE,
						pGENCOMHL_CTX->stSEND.stREQUEST.uiPROGR,
						&pGENCOMHL_CTX->stSEND.stREQUEST.aBuffer[0]);

				pGENCOMHL_CTX->stSEND.eStatusRetCode = GENCOMHL_STATUS_P_POSTING;
#ifdef DEBUG
				iRemainigTimeSND = 1000 * 60 * 2;
#else
				iRemainigTimeSND = 100 + ((int)pGENCOMHL_CTX->stSEND.stREQUEST.uiSIZE * 10);
#endif
				pGENCOMHL_CTX->stSEND.iTimeoutRunnung = iRemainigTimeSND;
				pGENCOMHL_CTX->stSEND.eSTATUS = GENCOMHL_SEND_POSTING;
			}
			else
			{
				iNextDeltaT_ms = 10;
			}
		}
		break;
	case GENCOMHL_SEND_POSTING:
		{
			if (uiEventMask & GENCOMHL_EVENT_ROUT_ANSW_SENT)
			{
				// La richiesta era in realt� una risposta, possibilmente il ritormo di un routing
				// completiamo
				pGENCOMHL_CTX->stSEND.eStatusRetCode = GENCOMHL_STATUS_SUCCESS;
				pGENCOMHL_CTX->stSEND.eSTATUS 		= GENCOMHL_SEND_COMPLETED;
				iRemainigTimeSND = 0;

			}
			else
			if (uiEventMask & GENCOMHL_EVENT_RQST_SENT)
			{
				iRemainigTimeSND = pGENCOMHL_CTX->stSEND.uiTimeOut;
				pGENCOMHL_CTX->stSEND.iTimeoutRunnung = iRemainigTimeSND;

				pGENCOMHL_CTX->stSEND.eStatusRetCode = GENCOMHL_STATUS_P_WAIT_ANSW;
				pGENCOMHL_CTX->stSEND.eSTATUS = GENCOMHL_SEND_WAIT_ANSWER;
			}
			else
			{
				pGENCOMHL_CTX->stSEND.iTimeoutRunnung-= deltaT_ms;

				iRemainigTimeSND = pGENCOMHL_CTX->stSEND.iTimeoutRunnung;
				if (iRemainigTimeSND <= 0)
				{
					pGENCOMHL_CTX->stSEND.eStatusRetCode = GENCOMHL_STATUS_P_POSTING_TO;
					pGENCOMHL_CTX->stSEND.eSTATUS = GENCOMHL_SEND_COMPLETED;
					iRemainigTimeSND = 0;
				}
			}
		}
		break;
	case GENCOMHL_SEND_WAIT_ANSWER:
		{
			if (uiEventMask & GENCOMHL_EVENT_ANSW_RECV)
			{
				pGENCOMHL_CTX->stSEND.eStatusRetCode = GENCOMHL_STATUS_SUCCESS;
				pGENCOMHL_CTX->stSEND.eSTATUS 		= GENCOMHL_SEND_COMPLETED;
				iRemainigTimeSND = 0;
			}
			else
			{
				pGENCOMHL_CTX->stSEND.iTimeoutRunnung -=deltaT_ms;
				iRemainigTimeSND = pGENCOMHL_CTX->stSEND.iTimeoutRunnung;
				if (iRemainigTimeSND <= 0)
				{
					pGENCOMHL_CTX->stERRORS.uiNO_ACK++;
					iRemainigTimeSND = 0;
					if (pGENCOMHL_CTX->stSEND.uiReties < pGENCOMHL_CTX->stSEND.uiRetiesMax)
					{
						pGENCOMHL_CTX->stSEND.eSTATUS = GENCOMHL_SEND_POST;
						pGENCOMHL_CTX->stSEND.uiReties++;

					}
					else
					{
						pGENCOMHL_CTX->stSEND.eStatusRetCode = GENCOMHL_STATUS_P_WAIT_ANSW_TO;
						pGENCOMHL_CTX->stSEND.eSTATUS = GENCOMHL_SEND_COMPLETED;
					}
				}
			}
		}
		break;
	case GENCOMHL_SEND_COMPLETED:
		{
			pGENCOMHL_CTX->stSEND.eSTATUS = GENCOMHL_SEND_COMPLETED_IDLE;
			pGENCOMHL_CTX->stCLBK.pfunct_sent(
					pGENCOMHL_CTX,
					pGENCOMHL_CTX->stCLBK.pctx_clbk,
					pGENCOMHL_CTX->stSEND.eStatusRetCode,
					&pGENCOMHL_CTX->stSEND.stREQUEST,
					&pGENCOMHL_CTX->stSEND.stANSW
					);
			iRemainigTimeRCV = 0;

		}
		break;
	case GENCOMHL_SEND_COMPLETED_IDLE:
		{
			uiSystemMustRun &= ~0x01;
		}
		break;
	}

	switch (pGENCOMHL_CTX->stRECV.eSTATUS)
	{
	case GENCOMHL_RECV_WAIT:
		{
			if (uiEventMask & GENCOMHL_EVENT_RQST_RECV)
			{
				bool bCompleted;
				pGENCOMHL_CTX->stRECV.eSTATUS = GENCOMHL_RECV_POST_ANSWER_WAIT;

				iRemainigTimeRCV = 0;

				pGENCOMHL_CTX->stRECV.stREQUEST.bNEW	= true;
				bCompleted = true;
//				if (pGENCOMHL_CTX->stRECV.stREQUEST.uiPROGR_LAST != pGENCOMHL_CTX->stRECV.stREQUEST.uiPROGR)
				{
					pGENCOMHL_CTX->stRECV.stREQUEST.uiPROGR_LAST = pGENCOMHL_CTX->stRECV.stREQUEST.uiPROGR;
					if (pGENCOMHL_CTX->stCLBK.pfunc_recv != NULL)
					{
						bCompleted = pGENCOMHL_CTX->stCLBK.pfunc_recv(
								pGENCOMHL_CTX,
								pGENCOMHL_CTX->stCLBK.pctx_clbk,
								pGENCOMHL_CTX->stRECV.stREQUEST.uiCODE,
								pGENCOMHL_CTX->stRECV.stREQUEST.uiSIZE,
								pGENCOMHL_CTX->stRECV.stREQUEST.uiPROGR,
								pGENCOMHL_CTX->stRECV.stREQUEST.aBuffer);

					}
				}
				if (bCompleted)
				{
					pGENCOMHL_CTX->stRECV.stREQUEST.bNEW	= false;
				}
				if (pGENCOMHL_CTX->stRECV.eSTATUS == GENCOMHL_RECV_POST_ANSWER_WAIT)
				{
#ifdef DEBUG
					iRemainigTimeRCV = 1000 * 60 * 2;
#else
					iRemainigTimeRCV = 500;
#endif
				}

				pGENCOMHL_CTX->stRECV.iTimeoutRunnung = iRemainigTimeRCV;

			}
			else
			{
				uiSystemMustRun &= ~0x10;
			}

		}
		break;
	case GENCOMHL_RECV_POST_ANSWER_WAIT:
		{
			{
				pGENCOMHL_CTX->stRECV.iTimeoutRunnung-=deltaT_ms;
				iRemainigTimeRCV = (int32_t)pGENCOMHL_CTX->stRECV.iTimeoutRunnung;
				if (iRemainigTimeRCV <= 0)
				{
					iRemainigTimeRCV = 0;

					pGENCOMHL_CTX->stRECV.stANSW_PENDING.uiCODE 	= pGENCOMHL_CTX->stRECV.stREQUEST.uiCODE;
					pGENCOMHL_CTX->stRECV.stANSW_PENDING.uiSIZE		= 2;
					pGENCOMHL_CTX->stRECV.stANSW_PENDING.aBuffer[0]	= GENCOMHL_ANSW_BAD_PROC;
					pGENCOMHL_CTX->stRECV.stANSW_PENDING.aBuffer[1]	= 0;
					pGENCOMHL_CTX->stRECV.stANSW_PENDING.uiCODE |= GENCOMHL_CMD_ANSWER_MASK;

					pGENCOMHL_CTX->stRECV.eSTATUS = GENCOMHL_RECV_POST_ANSWER;
				}
			}
		}
		break;
	case GENCOMHL_RECV_POST_ANSWER:
		{
			if (gencom_IsSendFree(pGENCOMHL_CTX->pGENCOM_CTX))
			{
				gencom_Send(
						pGENCOMHL_CTX->pGENCOM_CTX,
						GENCOM_SEND_ANSWER,
						pGENCOMHL_CTX->stRECV.stANSW_PENDING.uiCODE,
						pGENCOMHL_CTX->stRECV.stANSW_PENDING.uiSIZE,
						pGENCOMHL_CTX->stRECV.stANSW_PENDING.uiPROGR,
						&pGENCOMHL_CTX->stRECV.stANSW_PENDING.aBuffer[0]);

#ifdef DEBUG
				iRemainigTimeSND = 1000 * 60 * 2;
#else
				iRemainigTimeRCV = 100 + ((int)pGENCOMHL_CTX->stRECV.stANSW_PENDING.uiSIZE * 10);
#endif
				pGENCOMHL_CTX->stRECV.iTimeoutRunnung = iRemainigTimeRCV;
				pGENCOMHL_CTX->stRECV.eSTATUS = GENCOMHL_RECV_POSTING_ANSWER;
			}
			else
			{
				iNextDeltaT_ms = 10;
			}
		}
		break;
	case GENCOMHL_RECV_POSTING_ANSWER:
		{
			if (uiEventMask & GENCOMHL_EVENT_ANSW_SENT)
			{
				iRemainigTimeRCV = 0;
				pGENCOMHL_CTX->stRECV.eSTATUS = GENCOMHL_RECV_WAIT;
			}
			else
			{
				pGENCOMHL_CTX->stRECV.iTimeoutRunnung-=deltaT_ms;
				iRemainigTimeRCV = (int32_t)pGENCOMHL_CTX->stRECV.iTimeoutRunnung;
				if (iRemainigTimeRCV <= 0)
				{
					iRemainigTimeRCV = 0;
					pGENCOMHL_CTX->stRECV.eSTATUS = GENCOMHL_RECV_WAIT;
				}
			}

		}
		break;
	}
	if (iRemainigTimeSND >= 0)
	{
		iNextDeltaT_ms = iRemainigTimeSND;
	}
	if (iRemainigTimeRCV >= 0)
	{
		if (iNextDeltaT_ms < 0)
		{
			iNextDeltaT_ms = iRemainigTimeRCV;
		}
		if (iNextDeltaT_ms > iRemainigTimeRCV)
		{
			iNextDeltaT_ms = iRemainigTimeRCV;
		}
	}

//	*pbSytemRunning = true;
	if (uiSystemMustRun != 0)
	{
		*pbSytemRunning = true;
	}

	return iNextDeltaT_ms;
}


EGENCOMHL_STATUS_RET gencomhlPostRecvAnswer(GENCOMHL_CTX* pGENCOMHL_CTX,uint8_t uiCmd,unsigned uiSize,unsigned uiPROGR,const uint8_t* abyBuffer)
{
	EGENCOMHL_STATUS_RET	eRet;
	eRet =GENCOMHL_STATUS_INCONSISTENT;
	if (pGENCOMHL_CTX->stRECV.eSTATUS == GENCOMHL_RECV_POST_ANSWER_WAIT)
	{
		eRet =GENCOMHL_STATUS_INVAL;
		if (pGENCOMHL_CTX->stRECV.stREQUEST.uiCODE == uiCmd)
		{
			if (uiSize < GENCOMHL_ANSWER_MAX)
			{
				if (uiSize >= 1)
				{
					pGENCOMHL_CTX->stRECV.stANSW_PENDING.uiCODE 	= pGENCOMHL_CTX->stRECV.stREQUEST.uiCODE;
					pGENCOMHL_CTX->stRECV.stANSW_PENDING.uiCODE 	|= GENCOMHL_CMD_ANSWER_MASK;
					pGENCOMHL_CTX->stRECV.stANSW_PENDING.uiSIZE		= uiSize;
					pGENCOMHL_CTX->stRECV.stANSW_PENDING.uiPROGR	= uiPROGR;
					memcpy(&pGENCOMHL_CTX->stRECV.stANSW_PENDING.aBuffer[0],abyBuffer,uiSize);
					eRet =GENCOMHL_STATUS_SUCCESS;

					pGENCOMHL_CTX->stRECV.eSTATUS = GENCOMHL_RECV_POST_ANSWER;
				}
			}
		}
	}
	return eRet;
}


EGENCOMHL_STATUS_RET gencomhlPostSendRequest(GENCOMHL_CTX* pGENCOMHL_CTX,uint8_t uiOverrideProgr,uint8_t uiCmd,unsigned uiSizeHdr,const uint8_t* abyBufferHdr,unsigned uiSize,const uint8_t* abyBuffer,uint32_t uiTimeOut, unsigned uiRetiesMax)
{
	return gencomhlScheduleSendRequest(pGENCOMHL_CTX,-1,uiOverrideProgr,uiCmd,uiSizeHdr,abyBufferHdr,uiSize,abyBuffer,uiTimeOut,uiRetiesMax);
}

EGENCOMHL_STATUS_RET gencomhlScheduleSendRequest(GENCOMHL_CTX* pGENCOMHL_CTX,int iSchedulerSlot,uint8_t uiOverrideProgr,uint8_t uiCmd,unsigned uiSizeHdr,const uint8_t* abyBufferHdr,unsigned uiSize,const uint8_t* abyBuffer,uint32_t uiTimeOut, unsigned uiRetiesMax)
{
	EGENCOMHL_STATUS_RET	eRet;
	eRet = GENCOMHL_STATUS_BUSY;
	if ((iSchedulerSlot < 0) || (pGENCOMHL_CTX->pSENDER_SCHEDULER_CTX == NULL))
	{
		if (pGENCOMHL_CTX->stSEND.eSTATUS == GENCOMHL_SEND_FREE)
		{
			uint8_t	uiProgUsed;
			if (uiOverrideProgr > 0)
			{
				uiProgUsed = uiOverrideProgr;
			}
			else
			{
				pGENCOMHL_CTX->stSEND.uiRqstProgr++;
				if (pGENCOMHL_CTX->stSEND.uiRqstProgr == 0)
				{
					pGENCOMHL_CTX->stSEND.uiRqstProgr++;
				}
				uiProgUsed = pGENCOMHL_CTX->stSEND.uiRqstProgr;
			}

			pGENCOMHL_CTX->stSEND.stREQUEST.uiCODE = uiCmd;
			pGENCOMHL_CTX->stSEND.stREQUEST.uiSIZE = uiSizeHdr + uiSize;
			pGENCOMHL_CTX->stSEND.stREQUEST.uiPROGR = uiProgUsed;

			memcpy(&pGENCOMHL_CTX->stSEND.stREQUEST.aBuffer[0],abyBufferHdr,uiSizeHdr);
			memcpy(&pGENCOMHL_CTX->stSEND.stREQUEST.aBuffer[uiSizeHdr],abyBuffer,uiSize);

			pGENCOMHL_CTX->stSEND.uiTimeOut 	= pGENCOMHL_CTX->stSEND.stREQUEST.uiSIZE;
			pGENCOMHL_CTX->stSEND.uiTimeOut		/= 3;
			pGENCOMHL_CTX->stSEND.uiTimeOut 	+= uiTimeOut;
			pGENCOMHL_CTX->stSEND.uiReties	= 0;
			pGENCOMHL_CTX->stSEND.uiRetiesMax = uiRetiesMax;

			pGENCOMHL_CTX->stSEND.eSTATUS = GENCOMHL_SEND_POST;

			gencomhlFlushSendRequest(pGENCOMHL_CTX);

			eRet = GENCOMHL_STATUS_QUEUED;
		}
	}
	else
	{
		eRet = GENCOMHL_STATUS_INVALID_SCHEDULE;
		if ( iSchedulerSlot < pGENCOMHL_CTX->pSENDER_SCHEDULER_CTX->uiRequestMax)
		{
			const GENCOMHL_RQST_BLOCK* pRQST_BLOCK;
			pRQST_BLOCK = (const GENCOMHL_RQST_BLOCK*)&pGENCOMHL_CTX->pSENDER_SCHEDULER_CTX->aRQSTS[iSchedulerSlot];
			if ((pRQST_BLOCK != NULL) &&  (pRQST_BLOCK->pRQST != NULL))
			{
				GENCOMHL_RQST_BASE* pRQST;
				pRQST = (GENCOMHL_RQST_BASE*)(pRQST_BLOCK->pRQST);
				pRQST->stHDR.uiCODE 			= uiCmd;
				pRQST->stHDR.uiSIZE 			= uiSizeHdr + uiSize;
				pRQST->stHDR.uiRetries 			= uiRetiesMax;
				pRQST->stHDR.uiOverrideProgr	= uiOverrideProgr;
				pRQST->stHDR.uiTimeout 			= uiTimeOut;
				memcpy(&pRQST->auiPayLoad[0],abyBufferHdr,uiSizeHdr);
				memcpy(&pRQST->auiPayLoad[uiSizeHdr],abyBuffer,uiSize);

				pGENCOMHL_CTX->pSENDER_SCHEDULER_CTX->uiRQSTMASK_PENDING |= (1 << iSchedulerSlot);
				eRet = GENCOMHL_STATUS_SCHEDULED;
			}

		}
		gencomhlCheckAndSendPendingRequest(pGENCOMHL_CTX);
	}
	return eRet;
}

EGENCOMHL_STATUS_RET gencomhlCompleteSendRequest(GENCOMHL_CTX* pGENCOMHL_CTX,uint8_t* puiRetStatus,unsigned* puiRetSize,uint8_t* abyRetBuffer,bool bCancelled)
{
	EGENCOMHL_STATUS_RET eRet;
	if (bCancelled)
	{
		eRet = GENCOMHL_STATUS_CANCELLED;
	}
	else
	{
		if (pGENCOMHL_CTX->stSEND.eSTATUS != GENCOMHL_SEND_COMPLETED_IDLE)
		{
			eRet = GENCOMHL_STATUS_INCONSISTENT;
		}
		else
		{
			eRet = pGENCOMHL_CTX->stSEND.eStatusRetCode;
			if (eRet == GENCOMHL_STATUS_SUCCESS)
			{
				if (pGENCOMHL_CTX->stSEND.stANSW.uiSIZE == 0)
				{
					eRet = GENCOMHL_STATUS_INVALID_ANSWER_LENGTH;
				}
				else
				{
					if (puiRetStatus)
					{
						*puiRetStatus = pGENCOMHL_CTX->stSEND.stANSW.aBuffer[0];
					}
					if (puiRetSize && abyRetBuffer)
					{
						if (*puiRetSize > pGENCOMHL_CTX->stSEND.stANSW.uiSIZE)
						{
							*puiRetSize = pGENCOMHL_CTX->stSEND.stANSW.uiSIZE;
						}
						if (*puiRetSize > 0)
						{
							memcpy(abyRetBuffer,&pGENCOMHL_CTX->stSEND.stANSW.aBuffer[0],*puiRetSize);
						}
					}
				}
			}
		}
	}
	pGENCOMHL_CTX->stSEND.eSTATUS = GENCOMHL_SEND_FREE;
	{
		GENCOMHL_REQUEST_SENDER_CTX*	pSENDER_SCHEDULER_CTX;
		pSENDER_SCHEDULER_CTX = pGENCOMHL_CTX->pSENDER_SCHEDULER_CTX;
		if (pSENDER_SCHEDULER_CTX != NULL)
		{
			pSENDER_SCHEDULER_CTX->uiRQSTMASK_PENDING &= ~pSENDER_SCHEDULER_CTX->uiRQSTMASK_SENDING;
			pSENDER_SCHEDULER_CTX->uiRQSTMASK_SENDING = 0;
		}

	}
	return eRet;
}


bool gencomhlCheckAndStoreRecvFromBus(GENCOMHL_CTX* pGENCOMHL_CTX,uint8_t uiCmd, unsigned uiSize, unsigned uiPROGR,const uint8_t* abyBuffer,bool* pbIsAck)
{
	bool bIsValid;
	bIsValid = false;
	*pbIsAck = false;
	if (uiCmd & GENCOMHL_CMD_ANSWER_MASK)
	{
		if (pGENCOMHL_CTX->stSEND.stREQUEST.uiCODE == (uiCmd & GENCOMHL_CMD_CODE_MASK))
		{
			pGENCOMHL_CTX->stSEND.stANSW.uiCODE = uiCmd;
			pGENCOMHL_CTX->stSEND.stANSW.uiSIZE = uiSize;
			pGENCOMHL_CTX->stSEND.stANSW.uiPROGR = uiPROGR;
			memcpy(&pGENCOMHL_CTX->stSEND.stANSW.aBuffer[0],abyBuffer, uiSize);
			bIsValid = true;
			*pbIsAck = true;
		}
	}
	else
	{
		pGENCOMHL_CTX->stRECV.stREQUEST.uiCODE = uiCmd;
		pGENCOMHL_CTX->stRECV.stREQUEST.uiSIZE = uiSize;
		pGENCOMHL_CTX->stRECV.stREQUEST.uiPROGR = uiPROGR;
		memcpy(&pGENCOMHL_CTX->stRECV.stREQUEST.aBuffer[0],abyBuffer, uiSize);
		bIsValid = true;
	}
	return bIsValid;
}

bool gencomhlCheckAndSendPendingRequest(GENCOMHL_CTX* pGENCOMHL_CTX)
{
	GENCOMHL_REQUEST_SENDER_CTX*	pSENDER_SCHEDULER_CTX;
	pSENDER_SCHEDULER_CTX = pGENCOMHL_CTX->pSENDER_SCHEDULER_CTX;
	if (pSENDER_SCHEDULER_CTX != NULL)
	{
		if (pSENDER_SCHEDULER_CTX->uiRQSTMASK_SENDING == 0)
		{
			EGENCOMHL_STATUS_RET eRet;
			int			iSCNR_MAX;
			uint16_t	uiMASK;
			bool		bExitPosted;
			bool		bExitBusy;
			iSCNR_MAX = 16;


			bExitPosted = false;
			bExitBusy = false;
			do {
				if (pSENDER_SCHEDULER_CTX->uiRQS_SCNR >= pSENDER_SCHEDULER_CTX->uiRequestMax)
				{
					pSENDER_SCHEDULER_CTX->uiRQS_SCNR = 0;
				}
				uiMASK = 1 << pSENDER_SCHEDULER_CTX->uiRQS_SCNR;
				if (
						(pSENDER_SCHEDULER_CTX->uiRQSTMASK_PENDING & uiMASK)
						&&
						!(pSENDER_SCHEDULER_CTX->uiRQSTMASK_ERROR & uiMASK)
					)
				{
					const GENCOMHL_RQST_BLOCK* pRQST_BLOCK;
					bool	bRemovePending;
					bRemovePending = true;
					pRQST_BLOCK = (const GENCOMHL_RQST_BLOCK*)&pSENDER_SCHEDULER_CTX->aRQSTS[pSENDER_SCHEDULER_CTX->uiRQS_SCNR];
					if ((pRQST_BLOCK != NULL) &&  (pRQST_BLOCK->pRQST != NULL))
					{
						const GENCOMHL_RQST_BASE*	pRQST;
						pRQST = (const GENCOMHL_RQST_BASE*)pRQST_BLOCK->pRQST;

						bRemovePending	= false;

						eRet = gencomhlPostSendRequest(
								pGENCOMHL_CTX,
								pRQST->stHDR.uiOverrideProgr,
								pRQST->stHDR.uiCODE,
								0,
								NULL,
								pRQST->stHDR.uiSIZE,
								pRQST->auiPayLoad,
								pRQST->stHDR.uiTimeout,
								pRQST->stHDR.uiRetries);
						switch (eRet)
						{
						default:
							{
								pSENDER_SCHEDULER_CTX->uiRQSTMASK_ERROR |= uiMASK;
							}
							break;
						case GENCOMHL_STATUS_QUEUED:
							{
								pSENDER_SCHEDULER_CTX->uiRQSTMASK_SENDING = uiMASK;
								bExitPosted = true;
							}
							break;
						case GENCOMHL_STATUS_BUSY:
							{
								bExitBusy = true;
							}
							break;
						}
					}
					if (bRemovePending)
					{
						pSENDER_SCHEDULER_CTX->uiRQSTMASK_PENDING &= ~uiMASK;
					}
				}
				if (!bExitPosted)
				{
					pSENDER_SCHEDULER_CTX->uiRQS_SCNR++;
				}
				iSCNR_MAX--;
			}
			while ((iSCNR_MAX > 0) && (!bExitPosted) && (!bExitBusy));

			return bExitPosted;
		}
	}
	return false;
}

__weak void gencomhlFlushSendRequest(GENCOMHL_CTX* pGENCOMHL_CTX)
{

}
