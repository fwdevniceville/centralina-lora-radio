/*
 * bootmaster.c
 *
 *  Created on: 04 gen 2019
 *      Author: daniele_parise
 */
#include <stdbool.h>
#include <stdint.h>
#include "bootmaster.h"
#include "../intcom_hw/intcom_hw.h"
#include <string.h>
#include "../hwbrd/hw_bridge.h"
#include "low_power_manager.h"
#include "timeServer.h"
#include "hw_gpio.h"
#include "intcom/intcom.h"
#include "extcom/extcom.h"
#include "lora/lora_user.h"


typedef enum etagEBOOTMASTER_STATO{
	BOOTMASTER_INIT,
	BOOTMASTER_BOOT_WAIT,
	BOOTMASTER_STOPPED,
	BOOTMASTER_FREE_USER,

	BOOTMASTER_RESET_BOOT_START,
	BOOTMASTER_RESET_BOOT_RELEASE,

	BOOTMASTER_ROUTING
}EBOOTMASTER_STATO;

typedef struct tagBOOTMASTER_CTX{
	EBOOTMASTER_STATO	eSTATO;
	int					iSamplesTotal;
	int					iSamplesBootActive;
	int					iStdTimer;
	bool				bCompleted;
	bool				bBootRequest;
}BOOTMASTER_CTX;

BOOTMASTER_CTX	stBOOTMASTER_CTX;

TimerEvent_t timerBootMaster;


void bootmasterOnTimerSampler(void*);

void bootmasterInit(void)
{

}

void bootmasterOnTimerSampler(void* pctx)
{
	if (stBOOTMASTER_CTX.iStdTimer > 0)
	{
		stBOOTMASTER_CTX.iStdTimer--;
	}
	stBOOTMASTER_CTX.iSamplesTotal++;
	if (HW_GPIO_Read(GPIOA, GPIO_PIN_0 ))
	{
		stBOOTMASTER_CTX.iSamplesBootActive++;
	}
	if (stBOOTMASTER_CTX.iSamplesBootActive >= 10)
	{
		stBOOTMASTER_CTX.bCompleted = true;
		stBOOTMASTER_CTX.bBootRequest = true;
	}
	if (stBOOTMASTER_CTX.iSamplesTotal >= 100)
	{
		stBOOTMASTER_CTX.bCompleted = true;
	}
    TimerStart(&timerBootMaster);
}

void bootmasterMain(void)
{
	switch (stBOOTMASTER_CTX.eSTATO)
	{
	case BOOTMASTER_INIT:
		{
			GPIO_InitTypeDef initStruct={0};
			initStruct.Mode =GPIO_MODE_INPUT;
			initStruct.Pull = GPIO_PULLDOWN;
			initStruct.Speed = GPIO_SPEED_MEDIUM;
			HW_GPIO_Init( GPIOA, GPIO_PIN_0, &initStruct );

			stBOOTMASTER_CTX.iSamplesTotal = 0;
			stBOOTMASTER_CTX.iSamplesBootActive = 0;
			stBOOTMASTER_CTX.bCompleted = false;
			stBOOTMASTER_CTX.bBootRequest = false;


		    TimerInit( &timerBootMaster, bootmasterOnTimerSampler );
		    TimerSetValue( &timerBootMaster,  100);
		    TimerStart(&timerBootMaster);

			stBOOTMASTER_CTX.eSTATO = BOOTMASTER_BOOT_WAIT;
		}
		break;
	case BOOTMASTER_BOOT_WAIT:
		{
			if (stBOOTMASTER_CTX.bCompleted)
			{
				if (stBOOTMASTER_CTX.bBootRequest)
				{
					stBOOTMASTER_CTX.eSTATO = BOOTMASTER_FREE_USER;
				}
				else
				{
					GPIO_InitTypeDef initStruct={0};
					initStruct.Mode =GPIO_MODE_ANALOG;
					initStruct.Pull = GPIO_NOPULL;
					initStruct.Speed = GPIO_SPEED_LOW;
					HW_GPIO_Init( GPIOA, GPIO_PIN_0, &initStruct );

					stBOOTMASTER_CTX.eSTATO = BOOTMASTER_STOPPED;
					TimerStop(&timerBootMaster);
				}
			}
		}
		break;
	case BOOTMASTER_STOPPED:
		{

		}
		break;
	case BOOTMASTER_FREE_USER:
		{
		    GPIO_InitTypeDef GPIO_InitStruct={0};


			intcom_DeInit();
			extcom_DeInit();
			__HAL_RCC_GPIOA_CLK_ENABLE();
			__HAL_RCC_GPIOB_CLK_ENABLE();
			// GPIOB_PIN_2	= RESET
			// GPIOB_PIN_7	= BOOT
			// GPIOA_PIN_2	= EXTCOM_TX
			// GPIOA_PIN_3	= EXTCOM_RX

			// GPIOA_PIN_2	= EXTCOM_TX
			// GPIOA_PIN_3	= EXTCOM_RX

			// GPIOA_PIN_9	= INTCOM_TX
			// GPIOA_PIN_10	= INTCOM_RX

			MODIFY_REG(GPIOB->ODR,GPIO_PIN_2 | GPIO_PIN_7,GPIO_PIN_2);

			GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
			GPIO_InitStruct.Pull = GPIO_NOPULL;
			GPIO_InitStruct.Pin = GPIO_PIN_2 | GPIO_PIN_7;
			HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

			MODIFY_REG(GPIOA->ODR,GPIO_PIN_2 | GPIO_PIN_9, GPIO_PIN_2 | GPIO_PIN_9); // TX ALTI

			GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
			GPIO_InitStruct.Pull = GPIO_NOPULL;
			GPIO_InitStruct.Pin = GPIO_PIN_2 | GPIO_PIN_9;
			HAL_GPIO_Init(GPIOA, &GPIO_InitStruct); // TX PUSH PULL

			GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
			GPIO_InitStruct.Pull = GPIO_NOPULL;
			GPIO_InitStruct.Pin = GPIO_PIN_3 | GPIO_PIN_10;
			HAL_GPIO_Init(GPIOA, &GPIO_InitStruct); // RX INPUT

			LPM_SetStopMode(LPM_BOOTMGR_Id , LPM_Disable );

			lora_user_init_defaults();

			stBOOTMASTER_CTX.iStdTimer = 2;

			stBOOTMASTER_CTX.eSTATO = BOOTMASTER_RESET_BOOT_START;

		}
		break;
	case BOOTMASTER_RESET_BOOT_START:
		{
			if (stBOOTMASTER_CTX.iStdTimer <= 0)
			{
				MODIFY_REG(GPIOB->ODR,GPIO_PIN_2 | GPIO_PIN_7,GPIO_PIN_2 | GPIO_PIN_7);
				stBOOTMASTER_CTX.eSTATO = BOOTMASTER_RESET_BOOT_RELEASE;
			}
		}
		break;
	case BOOTMASTER_RESET_BOOT_RELEASE:
		{
			TimerStop(&timerBootMaster);
			stBOOTMASTER_CTX.eSTATO = BOOTMASTER_ROUTING;
		}
		break;
	case BOOTMASTER_ROUTING:
		{

			DISABLE_IRQ( );
			stBOOTMASTER_CTX.iSamplesBootActive = 1000000;

			while (stBOOTMASTER_CTX.iSamplesBootActive > 0)
			{
				if (GPIOA->IDR & GPIO_PIN_3)
				{
					GPIOA->ODR |= GPIO_PIN_9;
				}
				else
				{
					GPIOA->ODR &= ~GPIO_PIN_9;
				}

				if (GPIOA->IDR & GPIO_PIN_10)
				{
					GPIOA->ODR |= GPIO_PIN_2;
				}
				else
				{
					GPIOA->ODR &= ~GPIO_PIN_2;
				}
				if (!(GPIOA->IDR & GPIO_PIN_0))
				{
					if (stBOOTMASTER_CTX.iSamplesBootActive > 0)
					{
						stBOOTMASTER_CTX.iSamplesBootActive--;
					}
				}
			}




			ENABLE_IRQ( );

			stBOOTMASTER_CTX.eSTATO = BOOTMASTER_ROUTING;
		}
		break;
	}

}
