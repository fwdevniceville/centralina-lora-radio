/*
 * bootmaster.h
 *
 *  Created on: 04 gen 2019
 *      Author: daniele_parise
 */

#ifndef SRC_APPLICATION_BOOTMASTER_BOOTMASTER_H_
#define SRC_APPLICATION_BOOTMASTER_BOOTMASTER_H_

void bootmasterInit(void);
void bootmasterMain(void);

#endif /* SRC_APPLICATION_BOOTMASTER_BOOTMASTER_H_ */
