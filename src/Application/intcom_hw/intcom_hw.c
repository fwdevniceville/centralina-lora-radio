/*
 * intcom_hw.c
 *
 *  Created on: 14 mar 2018
 *      Author: daniele_parise
 */

#include <stdbool.h>
#include <stdint.h>
#include "../gencom/gencom.h"
#include "../extcom/extcom.h"
#include "../intcom/intcom.h"
#include "intcom_hw.h"
#include <string.h>
#include "../hwbrd/hw_bridge.h"
#include "low_power_manager.h"



void intcom_hwEnterStopMode(void)
{
	HAL_UARTEx_EnableStopMode(&UartIntcomHandle);
//	SET_BIT(UartIntcomHandle.Instance->CR1, USART_CR1_UESM);


}

void intcom_hwExitStopMode(void)
{

	HAL_UARTEx_DisableStopMode(&UartIntcomHandle);
//	CLEAR_BIT(UartIntcomHandle.Instance->CR1, USART_CR1_UESM);
}

