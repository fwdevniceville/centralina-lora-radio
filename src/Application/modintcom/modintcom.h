/*
 * modintcom.h
 *
 *  Created on: 12 mar 2018
 *      Author: daniele_parise
 */

#ifndef SRC_APPLICATION_MODINTCOM_MODINTCOM_H_
#define SRC_APPLICATION_MODINTCOM_MODINTCOM_H_

#include <gencom/gencom.h>
#include <gencom/gencomhl.h>

void modintcomInit(void);
void modintcomCheck(void);


void modintcomPulsEvent(void);


typedef enum etagEMODINTCOM_SCHED_SLOT{
	MODINTCOM_SCHED_SLOT_EVT_REBOOT,
	MODINTCOM_SCHED_SLOT_EVT_JOIN,
	MODINTCOM_SCHED_SLOT_EVT_CLASS,
	MODINTCOM_SCHED_SLOT_EVT_DOWNLINK,
	MODINTCOM_SCHED_SLOT_ROUTING_REQUEST,
	___MODINTCOM_SCHED_SLOT_MAX____
}EMODINTCOM_SCHED_SLOT;

extern GENCOMHL_CTX stINTCOMHL_CTX;

typedef void (*modintcomRouteCmd_callback_t)(void* pctx,EGENCOMHL_STATUS_RET eStautsRet, const struct tagGENCOMHL_MESSAGE_REQUEST* pRQST, const struct tagGENCOMHL_MESSAGE_ANSWER* pANSW,uint8_t uiPROGR_PENDING);

typedef struct tagMODINTCOMCTX{
	GENCOMHL_REQUEST_SENDER_CTX	stSEND_REQUESTS;
	uint32_t					uiTms_last;
	unsigned					uiEVT_MASK;
	struct {
		uint32_t	uiTms_now;
		uint8_t	auiBufferAnsw[128];
		unsigned uiAnswSize;

		unsigned uiProgressiveRepetitions;
		unsigned uiLoraErrors;
		unsigned uiMsgDelays;
		uint8_t	uiPROGRESSIVE;
	}stCURRENT_CMD;
	struct {
		uint32_t	uiTms_now;
		unsigned uiCmd;
		unsigned uiProgr;
		unsigned uiSize;
		uint8_t	abyBuffer[128];
	}stLAST_CMD;

	struct {
		modintcomRouteCmd_callback_t	pCallBack;
		void* 							pCallBackCtx;
		uint8_t	uiCMD_PENDING;
		uint8_t	uiPROGR_PENDING;
	}stROUTER;
	bool	bInSlotProcessing;
}MODINTCOMCTX;

extern MODINTCOMCTX	stMODINTCOMCTX;


bool modintcomIntcomhlRecvCallback(struct tagGENCOMHL_CTX* pGENCOMHL_CTX,void* pctx,uint8_t uiCmd, unsigned uiSize,unsigned uiProgr,const uint8_t* abyBuffer);
void modintcom_SendCmd_Clbk(struct tagGENCOMHL_CTX* pGENCOMHL_CTX,void* pctx,EGENCOMHL_STATUS_RET eStautsRet,const struct tagGENCOMHL_MESSAGE_REQUEST* pRQST, const struct tagGENCOMHL_MESSAGE_ANSWER* pANSW);
void intcomhlFlushSendRequest(void);

void modintcomIntcomhlDispatchLazyMessages(void);


EGENCOMHL_STATUS_RET modintcomRouteCmd(uint8_t uiCmd,uint8_t uiProgr, unsigned uiSize,const uint8_t* abyBuffer,modintcomRouteCmd_callback_t pCallBack,void* pctx);

#endif /* SRC_APPLICATION_MODINTCOM_MODINTCOM_H_ */
