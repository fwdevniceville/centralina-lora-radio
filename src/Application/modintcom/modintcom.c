/*
 * modintcom.c
 *
 *  Created on: 12 mar 2018
 *      Author: daniele_parise
 */

#include <stdbool.h>
#include <stdint.h>
#include <gencom/gencom.h>
#include <gencom/gencomhl.h>

#include "../modintcom/modintcom.h"
#include "../intcom/intcom.h"
#include "timeServer.h"
#include "hw.h"

#include "low_power_manager.h"
#include "../Application/lora/lora_user.h"

#include "../msgcom/msgcom.h"

#define MODINTCOMCTX_TEMP_BUFFER_SIZE	(128)

typedef struct tagMODINTCOM_RQST_DOWNLINK{
	GENCOMHL_RQST_HDR	stHDR;
	uint8_t				auiPayLoad[128];
}MODINTCOM_RQST_DOWNLINK;

typedef struct tagMODINTCOM_RQST_ROUTE{
	GENCOMHL_RQST_HDR	stHDR;
	uint8_t				auiPayLoad[512];
}MODINTCOM_RQST_ROUTE;


MODINTCOM_RQST_DOWNLINK	stMODINTCOM_RQST_DOWNLINK;
GENCOMHL_RQST_BASE		stMODINTCOM_RQST_EVT_REBOOT;
GENCOMHL_RQST_BASE		stMODINTCOM_RQST_EVT_JOIN;
GENCOMHL_RQST_BASE		stMODINTCOM_RQST_EVT_CLASS;
MODINTCOM_RQST_ROUTE	stMODINTCOM_RQST_ROUTE;

const GENCOMHL_RQST_BLOCK aMODINTCOM_RQSTS[___MODINTCOM_SCHED_SLOT_MAX____] = {
		{&stMODINTCOM_RQST_EVT_REBOOT.stHDR,sizeof(stMODINTCOM_RQST_EVT_REBOOT.auiPayLoad)},
		{&stMODINTCOM_RQST_EVT_JOIN.stHDR,sizeof(stMODINTCOM_RQST_EVT_JOIN.auiPayLoad)},
		{&stMODINTCOM_RQST_EVT_CLASS.stHDR,sizeof(stMODINTCOM_RQST_EVT_CLASS.auiPayLoad)},
		{&stMODINTCOM_RQST_DOWNLINK.stHDR,sizeof(stMODINTCOM_RQST_DOWNLINK.auiPayLoad)},
		{&stMODINTCOM_RQST_ROUTE.stHDR,sizeof(stMODINTCOM_RQST_ROUTE.auiPayLoad)}
};

MODINTCOMCTX	stMODINTCOMCTX;

TimerEvent_t timerModCom;
void OnTimerModComEvent(  void* context  );

bool modintcomOnBusRxCallBack(GENCOM_CTX* pGENCOM_CTX,uint8_t uiCmd, unsigned uiSize,unsigned uiProgr,const uint8_t* abyBuffer);
bool modintcomOnBusTxcCallBack(GENCOM_CTX* pGENCOM_CTX,EGENCOM_SEND_REASON eReason,uint8_t uiCmd, unsigned ,unsigned uiProgr,const uint8_t* abyBuffer);
void modintcomProcessSendRequests(void);

GENCOMHL_CTX stINTCOMHL_CTX;

void modintcomInit(void)
{
	stMODINTCOMCTX.stSEND_REQUESTS.aRQSTS 		= &aMODINTCOM_RQSTS[0];
	stMODINTCOMCTX.stSEND_REQUESTS.uiRequestMax = sizeof(aMODINTCOM_RQSTS) / sizeof(aMODINTCOM_RQSTS[0]);

	lora_user_Init();
	intcom_Init(modintcomOnBusRxCallBack,modintcomOnBusTxcCallBack);
	gencomhlInit(&stINTCOMHL_CTX,&stINTCOM_CTX,modintcomIntcomhlRecvCallback,modintcom_SendCmd_Clbk,NULL,&stMODINTCOMCTX.stSEND_REQUESTS);


	TimerInit( &timerModCom, OnTimerModComEvent );

}

void modintcomFree(void)
{
	gencomhlFree(&stINTCOMHL_CTX);
	intcom_DeInit();
	TimerStop(&timerModCom);
}

void modintcomCheck(void)
{
//	modintcomProcessSendRequests();
	modintcomIntcomhlDispatchLazyMessages();
}





void modintcomProcessSendRequests(void)
{
/*
	if (intcom_IsSendFree())
	{
		if (stMODINTCOMCTX.stPENDING_ACK_SEND.uiPENDING > 0)
		{
			intcom_Send(stMODINTCOMCTX.stPENDING_ACK_SEND.uiCODE,stMODINTCOMCTX.stPENDING_ACK_SEND.uiSize, &stMODINTCOMCTX.stPENDING_ACK_SEND.auiBUFFER[0]);
			stMODINTCOMCTX.stPENDING_ACK_SEND.uiPENDING = 0;
		}
		else
		if (stMODINTCOMCTX.stPENDING_TX_SEND.uiPENDING > 0)
		{
			intcom_Send(stMODINTCOMCTX.stPENDING_TX_SEND.uiCODE,stMODINTCOMCTX.stPENDING_TX_SEND.uiSize, &stMODINTCOMCTX.stPENDING_TX_SEND.auiBUFFER[0]);
			stMODINTCOMCTX.stPENDING_TX_SEND.uiPENDING = 0;
		}
	}
*/
}


bool modintcomOnBusRxCallBack(GENCOM_CTX* pGENCOM_CTX,uint8_t uiCmd, unsigned uiSize,unsigned uiProgr,const uint8_t* abyBuffer)
{
	unsigned uxBitsToSet;
	bool		bValid;
	bool		bIsAck;
	uxBitsToSet = 0;
	bIsAck = false;
	BACKUP_PRIMASK();
	DISABLE_IRQ( );


	bValid = gencomhlCheckAndStoreRecvFromBus(&stINTCOMHL_CTX,uiCmd,uiSize,uiProgr,abyBuffer,&bIsAck);
	if (bValid)
	{
		if (bIsAck)
		{
			uxBitsToSet = GENCOMHL_EVENT_ANSW_RECV;

		}
		else
		{
			uxBitsToSet = GENCOMHL_EVENT_RQST_RECV;
		}
	}

	bValid = false;
	if (uxBitsToSet != 0)
	{
		stMODINTCOMCTX.uiEVT_MASK |= uxBitsToSet;
		TimerStop(&timerModCom);
//		TimerSetValue(&timerModCom,1);
//		TimerStart(&timerModCom);
		// acceleriamo
		OnTimerModComEvent(NULL);
		bValid = true;
	}
    RESTORE_PRIMASK( );

	return bValid;
}

bool modintcomOnBusTxcCallBack(GENCOM_CTX* pGENCOM_CTX,EGENCOM_SEND_REASON eReason,uint8_t uiCmd, unsigned uiSize,unsigned uiProgr,const uint8_t* abyBuffer)
{
	unsigned uxBitsToSet;
	bool		bRet;
	BACKUP_PRIMASK();
	DISABLE_IRQ( );

	if (uiCmd & GENCOMHL_CMD_ANSWER_MASK)
	{
		uxBitsToSet = GENCOMHL_EVENT_ANSW_SENT;
	}
	else
	{
		uxBitsToSet = GENCOMHL_EVENT_RQST_SENT;
	}
	bRet = false;
	if (uxBitsToSet != 0)
	{
		stMODINTCOMCTX.uiEVT_MASK |= uxBitsToSet;
		TimerStop(&timerModCom);
//		TimerSetValue(&timerModCom,1);
//		TimerStart(&timerModCom);
		// acceleriamo
		OnTimerModComEvent(NULL);
		bRet = true;
	}
    RESTORE_PRIMASK( );
	return bRet;
}

void intcomhlFlushSendRequest(void)
{
	OnTimerModComEvent(NULL);
}

void OnTimerModComEvent( void* context )
{
	uint32_t	uiTTicks;
	uint32_t	uiTms_now;
	uint32_t	uiTms_delta;
	int			iNextDelta;
	int			iIterMax;
	bool		bSysRunning;

	if (stMODINTCOMCTX.bInSlotProcessing)
	{
		return;
	}

	BACKUP_PRIMASK();
	DISABLE_IRQ( );
	if (!stMODINTCOMCTX.bInSlotProcessing)
	{
		stMODINTCOMCTX.bInSlotProcessing = true;
		uiTTicks = HW_RTC_GetTimerValue();
		uiTms_now = HW_RTC_Tick2ms(uiTTicks);

		uiTms_delta = uiTms_now - stMODINTCOMCTX.uiTms_last;
		stMODINTCOMCTX.uiTms_last = uiTms_now;

		iIterMax = 10;

		do
		{
			bSysRunning = false;
			iNextDelta = gencomhlExecComunicationSlot(&stINTCOMHL_CTX,uiTms_delta,stMODINTCOMCTX.uiEVT_MASK,&bSysRunning);
			uiTms_delta = 0;
			stMODINTCOMCTX.uiEVT_MASK = 0;
			iIterMax--;
		}
		while ((iNextDelta == 0) && (iIterMax > 0));

		LPM_SetStopMode(LPM_INTCOMHL_Id , bSysRunning ? LPM_Disable : LPM_Enable );

		TimerStop(&timerModCom);
		if (iNextDelta >= 0)
		{
			TimerSetValue(&timerModCom,iNextDelta);
			TimerStart(&timerModCom);
		}
		stMODINTCOMCTX.bInSlotProcessing = false;

	}
    RESTORE_PRIMASK( );
}


EGENCOMHL_STATUS_RET modintcom_SendCmd(uint8_t uiCmd, unsigned uiSize,const uint8_t* abyBuffer,uint32_t uiTimeOut);
EGENCOMHL_STATUS_RET modintcom_SendCmd(uint8_t uiCmd, unsigned uiSize,const uint8_t* abyBuffer,uint32_t uiTimeOut)
{

	EGENCOMHL_STATUS_RET eRet;
	eRet = GENCOMHL_STATUS_INVAL;

	if (uiSize <= GENCOMHL_REQUEST_MAX)
	{

//		osMutexWait(mutexIntComHandle, osWaitForever);
		BACKUP_PRIMASK();
		DISABLE_IRQ( );


		eRet = gencomhlPostSendRequest(&stINTCOMHL_CTX,0,uiCmd,uiSize,abyBuffer,0,NULL,uiTimeOut,3);
		if (eRet == GENCOMHL_STATUS_SUCCESS)
		{
			TimerStop(&timerModCom);
			TimerSetValue(&timerModCom,1);
			TimerStart(&timerModCom);
		}
	    RESTORE_PRIMASK( );
//		osMutexRelease(mutexIntComHandle);
	}
	return eRet;
}

void modintcom_SendCmd_Clbk(GENCOMHL_CTX* pGENCOMHL_CTX,void* pctx,EGENCOMHL_STATUS_RET eStautsRet,const struct tagGENCOMHL_MESSAGE_REQUEST* pRQST, const struct tagGENCOMHL_MESSAGE_ANSWER* pANSW)
{
//	osMutexWait(mutexIntComHandle, portMAX_DELAY);
	EGENCOMHL_STATUS_RET	eRet;
	uint8_t uiRetStatus;
	unsigned uiRetSize;
	uint8_t abyRetBuffer[128];
	bool	bCancelled;
	bCancelled = false;

	BACKUP_PRIMASK();
	DISABLE_IRQ( );

	eRet = gencomhlCompleteSendRequest(&stINTCOMHL_CTX,&uiRetStatus,&uiRetSize,abyRetBuffer,bCancelled);
	if (stMODINTCOMCTX.stROUTER.uiCMD_PENDING == pRQST->uiCODE)
	{
		if (stMODINTCOMCTX.stROUTER.pCallBack != NULL)
		{
			stMODINTCOMCTX.stROUTER.pCallBack(stMODINTCOMCTX.stROUTER.pCallBackCtx,eRet,pRQST,pANSW,stMODINTCOMCTX.stROUTER.uiPROGR_PENDING);
		}
	}
	if (eRet == GENCOMHL_STATUS_SUCCESS)
	{
	}


	// next request.............
	gencomhlCheckAndSendPendingRequest(&stINTCOMHL_CTX);


    RESTORE_PRIMASK( );

//	xEventGroupSetBits(evtgrpIntComFunctionsCompletedHandle,TASKINCOM_EVENT_FUNCTION_COMPLETED_SEND);
}
void lora_user_OnEvent(uint8_t uiEvent,uint8_t uiData)
{
	int	iScheduler;
	uint8_t	auiBUFF[2];
	iScheduler = -1;
	switch (uiEvent)
	{
	case LORA_USER_EVENT_REBOOT:
		auiBUFF[0] =LORA_USER_EVENT_REBOOT;
		iScheduler = MODINTCOM_SCHED_SLOT_EVT_REBOOT;
		break;
	case LORA_USER_EVENT_JOINED:
		auiBUFF[0] =LORA_USER_EVENT_JOINED;
		iScheduler = MODINTCOM_SCHED_SLOT_EVT_JOIN;
		break;
	case LORA_USER_EVENT_CLASS_CONFIRMED:
		auiBUFF[0] =LORA_USER_EVENT_CLASS_CONFIRMED;
		iScheduler = MODINTCOM_SCHED_SLOT_EVT_CLASS;
		break;
	}
	if (iScheduler >= 0)
	{
		auiBUFF[1] =uiData;
		gencomhlScheduleSendRequest(&stINTCOMHL_CTX,iScheduler,0,MODINTCOM_CMD_SEND_EVENT,2,auiBUFF,0,NULL,100,3);
	}
}



void lora_user_OnRxData(const LoraUserRxInfos* pInfos, uint8_t Port,uint8_t BuffSize,uint8_t* Buff)
{
	uint8_t	aHdr[12];

	aHdr[0] = pInfos->uiDownLinkCounter >> 24;
	aHdr[1] = pInfos->uiDownLinkCounter >> 16;
	aHdr[2] = pInfos->uiDownLinkCounter >> 8;
	aHdr[3] = pInfos->uiDownLinkCounter >> 0;

	aHdr[4] = pInfos->Rssi >> 8;
	aHdr[5] = pInfos->Rssi >> 0;

	aHdr[6] = pInfos->Snr;

	aHdr[7] = pInfos->bFramePending;

	aHdr[8] = Port;
	aHdr[9] = 0; // confirm request
	aHdr[10] = BuffSize;

//	ss
	gencomhlScheduleSendRequest(&stINTCOMHL_CTX,MODINTCOM_SCHED_SLOT_EVT_DOWNLINK,0,MODINTCOM_CMD_SEND_DOWNLINK,11,aHdr,BuffSize,Buff,1000,3);
}


void modintcomPulsEvent(void)
{
	EGENCOMHL_STATUS_RET	eRet;
	uint8_t					aData[4];
	aData[0]= 0x01;
	aData[1]= 0x02;
	aData[2]= 0x03;
	aData[3]= 0x04;

	eRet = modintcom_SendCmd(3,4,aData,1000);
}

EGENCOMHL_STATUS_RET modintcomRouteCmd(uint8_t uiCmd,uint8_t uiProgr, unsigned uiSize,const uint8_t* abyBuffer,modintcomRouteCmd_callback_t pCallBack,void* pctx)
{
	EGENCOMHL_STATUS_RET	eRET;
	eRET = gencomhlScheduleSendRequest(&stINTCOMHL_CTX,MODINTCOM_SCHED_SLOT_ROUTING_REQUEST,0,uiCmd,uiSize,abyBuffer,0,NULL,200,0);
	if (eRET == GENCOMHL_STATUS_SCHEDULED)
	{
		stMODINTCOMCTX.stROUTER.uiCMD_PENDING 	= uiCmd;
		stMODINTCOMCTX.stROUTER.uiPROGR_PENDING	= uiProgr;
		stMODINTCOMCTX.stROUTER.pCallBack 		= pCallBack;
		stMODINTCOMCTX.stROUTER.pCallBackCtx 	= pctx;
	}
	return eRET;
}
