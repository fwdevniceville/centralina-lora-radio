

#include <stdbool.h>
#include <stdint.h>

#include <gencom/gencom.h>
#include <gencom/gencomhl.h>
#include "../modintcom/modintcom.h"
#include "../intcom/intcom.h"
#include "../Application/lora/lora_user.h"
#include "../msgcom/msgcom.h"
#include "LoRaMac.h"
#include <string.h>
#include "hw_rtc.h"
/*
#include "timeServer.h"
#include "hw.h"

#include "low_power_manager.h"
*/
void modintcomIntcomhlParseDevInfos(const uint8_t *abyBuffer, unsigned uiDevInfoSize);

bool modintcomIntcomhlRecvCallback_internal(GENCOMHL_CTX* pGENCOMHL_CTX,void* pctx,uint8_t uiCmd, unsigned uiSize,unsigned uiProgr,const uint8_t* abyBuffer);

bool modintcomIntcomhlRecvCallback(GENCOMHL_CTX* pGENCOMHL_CTX,void* pctx,uint8_t uiCmd, unsigned uiSize,unsigned uiProgr,const uint8_t* abyBuffer)
{
	return false;
//	return modintcomIntcomhlRecvCallback_internal(pGENCOMHL_CTX,pctx,uiCmd,uiSize,uiProgr,abyBuffer);
}

void modintcomIntcomhlDispatchLazyMessages(void)
{
	if (stINTCOMHL_CTX.stRECV.stREQUEST.bNEW)
	{
		modintcomIntcomhlRecvCallback_internal(
				&stINTCOMHL_CTX,
				stINTCOMHL_CTX.stCLBK.pctx_clbk,
				stINTCOMHL_CTX.stRECV.stREQUEST.uiCODE,
				stINTCOMHL_CTX.stRECV.stREQUEST.uiSIZE,
				stINTCOMHL_CTX.stRECV.stREQUEST.uiPROGR,
				stINTCOMHL_CTX.stRECV.stREQUEST.aBuffer);
		stINTCOMHL_CTX.stRECV.stREQUEST.bNEW = false;
	}

}

bool modintcomIntcomhlRecvCallback_internal(GENCOMHL_CTX* pGENCOMHL_CTX,void* pctx,uint8_t uiCmd, unsigned uiSize,unsigned uiProgr,const uint8_t* abyBuffer)
{
	LoRaMacCurrentTxInfos_t	stLoRaMacCurrentTxInfos;
	uint32_t	uiTTicks;
	uint32_t	uiTms_now;
	bool bCompleted;
	bCompleted = true;

	uiTTicks = HW_RTC_GetTimerValue();
	uiTms_now = HW_RTC_Tick2ms(uiTTicks);

	if (stMODINTCOMCTX.stCURRENT_CMD.uiPROGRESSIVE != uiProgr)
	{
		stMODINTCOMCTX.stCURRENT_CMD.uiPROGRESSIVE = uiProgr;
	}
	else
	{
		stMODINTCOMCTX.stCURRENT_CMD.uiProgressiveRepetitions++;

	}
	stMODINTCOMCTX.stCURRENT_CMD.uiTms_now = uiTms_now;

	stMODINTCOMCTX.stCURRENT_CMD.uiAnswSize = 2;
	memset(stMODINTCOMCTX.stCURRENT_CMD.auiBufferAnsw,0,sizeof(stMODINTCOMCTX.stCURRENT_CMD.auiBufferAnsw));
	memset(&stLoRaMacCurrentTxInfos,0,sizeof(stLoRaMacCurrentTxInfos));

	stMODINTCOMCTX.stCURRENT_CMD.auiBufferAnsw[0] =GENCOMHL_ANSW_CMD_UNK;
	switch (uiCmd)
	{
	case MODINTCOM_CMD_SET_LORA_PARAMS:
		{
			LORAWAN_PARAMS	stLoRaWAN;

			// copia selvatica dei parametri

			stLoRaWAN		= *(LORAWAN_PARAMS*)&abyBuffer[0];

#ifdef MAIN_STANDALONE
			lora_user_force_ok();
			stMODINTCOMCTX.stCURRENT_CMD.auiBufferAnsw[0] = LORAWAN_STATUS_OK;
#else
			stMODINTCOMCTX.stCURRENT_CMD.auiBufferAnsw[0] = lora_userSetParams(&stLoRaWAN);
#endif
		}
		break;
	case MODINTCOM_CMD_ENABLE_LORA:
		{
			stMODINTCOMCTX.stCURRENT_CMD.auiBufferAnsw[0] =GENCOMHL_ANSW_CMD_OK;
			if (abyBuffer[0])
			{
#ifdef MAIN_STANDALONE
				lora_user_force_ok();
				stMODINTCOMCTX.stCURRENT_CMD.auiBufferAnsw[0] = LORAWAN_STATUS_OK;
#else
				stMODINTCOMCTX.stCURRENT_CMD.auiBufferAnsw[0] = lora_userJoin(abyBuffer[1],&stLoRaMacCurrentTxInfos);
#endif
				stMODINTCOMCTX.stCURRENT_CMD.auiBufferAnsw[1] = 1;
				stMODINTCOMCTX.stCURRENT_CMD.auiBufferAnsw[2] = stLoRaMacCurrentTxInfos.timeNextTxDelay >> 24;
				stMODINTCOMCTX.stCURRENT_CMD.auiBufferAnsw[3] = stLoRaMacCurrentTxInfos.timeNextTxDelay >> 16;
				stMODINTCOMCTX.stCURRENT_CMD.auiBufferAnsw[4] = stLoRaMacCurrentTxInfos.timeNextTxDelay >> 8;
				stMODINTCOMCTX.stCURRENT_CMD.auiBufferAnsw[5] = stLoRaMacCurrentTxInfos.timeNextTxDelay >> 0;
				stMODINTCOMCTX.stCURRENT_CMD.auiBufferAnsw[6] = stLoRaMacCurrentTxInfos.dataRate;
				stMODINTCOMCTX.stCURRENT_CMD.auiBufferAnsw[7] = stLoRaMacCurrentTxInfos.powerTx;
				stMODINTCOMCTX.stCURRENT_CMD.auiBufferAnsw[8] = stLoRaMacCurrentTxInfos.uiUplinkCounter >> 8;
				stMODINTCOMCTX.stCURRENT_CMD.auiBufferAnsw[9] = stLoRaMacCurrentTxInfos.uiUplinkCounter >> 0;
				stMODINTCOMCTX.stCURRENT_CMD.auiBufferAnsw[10] = stLoRaMacCurrentTxInfos.timeOnAirTx >> 8;
				stMODINTCOMCTX.stCURRENT_CMD.auiBufferAnsw[11] = stLoRaMacCurrentTxInfos.timeOnAirTx >> 0;

				stMODINTCOMCTX.stCURRENT_CMD.uiAnswSize = 12;

			}
			else
			{
				stMODINTCOMCTX.stCURRENT_CMD.auiBufferAnsw[1] = 0;
			}

		}
		break;
	case MODINTCOM_CMD_RADIO_TX_TEST:
		{
			stMODINTCOMCTX.stCURRENT_CMD.auiBufferAnsw[0] =GENCOMHL_ANSW_CMD_INVALID_PARAMS;
			if (uiSize >= 8)
			{
				uint32_t Frequency;
				uint8_t Power;
				uint16_t Timeout;
				uint8_t		uiLoraRet;
				Timeout = ( uint16_t )( (abyBuffer[0] << 8) | abyBuffer[1]);
				Power = ( uint16_t )( (abyBuffer[2] << 8) | abyBuffer[3]);
				Frequency = ( uint32_t )( (abyBuffer[4] << 24) | (abyBuffer[5] << 16) | (abyBuffer[6] << 8) | abyBuffer[7]);

				uiLoraRet = LORA_TXContinuosWave(Frequency,Power,Timeout);

				stMODINTCOMCTX.stCURRENT_CMD.uiAnswSize = 12;
				stMODINTCOMCTX.stCURRENT_CMD.auiBufferAnsw[0] = uiLoraRet;
				stMODINTCOMCTX.stCURRENT_CMD.auiBufferAnsw[1] = 0;
			}
		}
		break;
	case MODINTCOM_CMD_SEND_UPLINK:
		{
			uint8_t		uiLoraRet;
			unsigned 	uiSizePosition;
			unsigned 	uiPayPosition;
			unsigned 	uiDevInfoSize;
			bool		bConfirmMsg;
			stMODINTCOMCTX.stCURRENT_CMD.auiBufferAnsw[0] =GENCOMHL_ANSW_CMD_INVALID_PARAMS;

#ifdef DEBUG
			if (abyBuffer[0] == 11)
			{
				volatile	int iBreak;
				iBreak = 0;

			}
#endif
			//!! TODO: verifica di coerenza parametri
			{
				int	eLoRaMacStatus;
				uiSizePosition= 2;
				uiPayPosition = 3;
				bConfirmMsg = false;
				if (abyBuffer[1] & 0x80)
				{
					bConfirmMsg = true;
				}
				uiDevInfoSize = abyBuffer[1] & 0xF;
				uiSizePosition += uiDevInfoSize;
				uiPayPosition = uiSizePosition + 1;

				if (uiDevInfoSize > 0)
				{
					modintcomIntcomhlParseDevInfos(&abyBuffer[2],uiDevInfoSize);
				}


				uiLoraRet = lora_user_SendData(
							abyBuffer[0],	//unsigned uiPort,
							bConfirmMsg,	//bool bConfirmNeeded,
							abyBuffer[uiSizePosition],	//unsigned uiSize,
							&abyBuffer[uiPayPosition],	//const uint8_t* abyBuffer
							&stLoRaMacCurrentTxInfos,
							&eLoRaMacStatus
						);
				if (eLoRaMacStatus != 0)
				{
					stMODINTCOMCTX.stCURRENT_CMD.uiLoraErrors++;
				}
				if (stLoRaMacCurrentTxInfos.timeNextTxDelay > 0)
				{
					stMODINTCOMCTX.stCURRENT_CMD.uiMsgDelays++;
				}
				stMODINTCOMCTX.stCURRENT_CMD.auiBufferAnsw[0] = uiLoraRet;
				stMODINTCOMCTX.stCURRENT_CMD.auiBufferAnsw[1] = 0;
				stMODINTCOMCTX.stCURRENT_CMD.auiBufferAnsw[2] = stLoRaMacCurrentTxInfos.timeNextTxDelay >> 24;
				stMODINTCOMCTX.stCURRENT_CMD.auiBufferAnsw[3] = stLoRaMacCurrentTxInfos.timeNextTxDelay >> 16;
				stMODINTCOMCTX.stCURRENT_CMD.auiBufferAnsw[4] = stLoRaMacCurrentTxInfos.timeNextTxDelay >> 8;
				stMODINTCOMCTX.stCURRENT_CMD.auiBufferAnsw[5] = stLoRaMacCurrentTxInfos.timeNextTxDelay >> 0;
				stMODINTCOMCTX.stCURRENT_CMD.auiBufferAnsw[6] = stLoRaMacCurrentTxInfos.dataRate;
				stMODINTCOMCTX.stCURRENT_CMD.auiBufferAnsw[7] = stLoRaMacCurrentTxInfos.powerTx;
				stMODINTCOMCTX.stCURRENT_CMD.auiBufferAnsw[8] = stLoRaMacCurrentTxInfos.uiUplinkCounter >> 8;
				stMODINTCOMCTX.stCURRENT_CMD.auiBufferAnsw[9] = stLoRaMacCurrentTxInfos.uiUplinkCounter >> 0;
				stMODINTCOMCTX.stCURRENT_CMD.auiBufferAnsw[10] = stLoRaMacCurrentTxInfos.timeOnAirTx >> 8;
				stMODINTCOMCTX.stCURRENT_CMD.auiBufferAnsw[11] = stLoRaMacCurrentTxInfos.timeOnAirTx >> 0;

				stMODINTCOMCTX.stCURRENT_CMD.uiAnswSize = 12;
			}
		}
		break;
	}

	gencomhlPostRecvAnswer(pGENCOMHL_CTX,uiCmd,stMODINTCOMCTX.stCURRENT_CMD.uiAnswSize, uiProgr,stMODINTCOMCTX.stCURRENT_CMD.auiBufferAnsw);

#ifdef DEBUG
	{
		unsigned uiUseSize;
		uiUseSize = uiSize;
		stMODINTCOMCTX.stLAST_CMD.uiTms_now = uiTms_now;
		stMODINTCOMCTX.stLAST_CMD.uiCmd = uiCmd;
		stMODINTCOMCTX.stLAST_CMD.uiProgr = uiProgr;
		stMODINTCOMCTX.stLAST_CMD.uiSize = uiUseSize;
		if (uiUseSize > sizeof(stMODINTCOMCTX.stLAST_CMD.abyBuffer))
		{
			uiUseSize = sizeof(stMODINTCOMCTX.stLAST_CMD.abyBuffer);
		}
		memcpy(stMODINTCOMCTX.stLAST_CMD.abyBuffer,abyBuffer,uiUseSize);

	}
#endif

	return bCompleted;
}

#include "lora/lora_hw_scv.h"
void modintcomIntcomhlParseDevInfos(const uint8_t *abyBuffer, unsigned uiDevInfoSize)
{
	if (uiDevInfoSize > 0)
	{
		stLORA_HW_SVC_DEV_INFOS.uiBatteryLevel = abyBuffer[0];
		uiDevInfoSize--;
		abyBuffer++;
	}
	if (uiDevInfoSize > 0)
	{
		uiDevInfoSize--;
	}

}

