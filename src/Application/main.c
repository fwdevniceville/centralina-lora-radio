/******************************************************************************
  * @file    main.c
  * @author  MCD Application Team
  * @version V1.1.4
  * @date    08-January-2018
  * @brief   this is the main!
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2017 STMicroelectronics International N.V. 
  * All rights reserved.</center></h2>
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "hw.h"
#include "low_power_manager.h"
#include "lora.h"
#include "timeServer.h"
#include "vcom.h"
#include "version.h"
#include "modintcom/modintcom.h"
#if (USE_EXT_COMM())
#include "modextcom/modextcom.h"
#endif
#include "timeServer/timeSrvCheck.h"
#include "lora/lora_user.h"
#ifdef MAIN_STANDALONE
#include "standalone/standalone.h"
#endif
#include "bootmaster/bootmaster.h"
/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/


/* !
 *Initialises the Lora Parameters
 */

/* Private functions ---------------------------------------------------------*/
void HW_IoDeInit( void );

/**
  * @brief  Main program
  * @param  None
  * @retval None
  */
int main( void )
{
	/* STM32 HAL library initialization*/
	HAL_Init( );
  
  	/* Configure the system clock*/
	SystemClock_Config( );
  
	/* Configure the debug mode*/
	DBG_Init( );
  
	/* Configure the hardware*/
	HW_Init( );
  
	/* USER CODE BEGIN 1 */
	/* USER CODE END 1 */

	/*Disbale Stand-by mode*/
	LPM_SetOffMode(LPM_APPLI_Id , LPM_Disable );

#if 0

	lora_user_init_defaults();
	HW_IoDeInit( );
	while (1)
	{
		  /* Enter Stop Mode */
		HAL_PWR_EnterSTOPMode ( PWR_LOWPOWERREGULATOR_ON, PWR_STOPENTRY_WFI );
	}

#endif

	modintcomInit();
#if (USE_EXT_COMM())
	modextcomInit();
#endif

	bootmasterInit();

	timerServerCheckInit();
	/* Configure the Lora Stack*/

	lora_user_init_defaults();
	//LORA_Init( &LoRaMainCallbacks, &LoRaParamInit);

  
	PRINTF("VERSION: %X\n\r", VERSION);
#ifdef MAIN_STANDALONE
	standaloneInit();
#endif

	//LORA_Join( );
  
	//LoraStartTx( TX_ON_EVENT) ;
  
	lora_user_OnEvent(LORA_USER_EVENT_REBOOT,0);

	while( 1 )
	{
		if (stLORA_USER_CTX.bLoraMACProcRqst)
		{
			stLORA_USER_CTX.bLoraMACProcRqst = false;
			LoRaMacProcess( );
		}
		timerServerCheck();
		modintcomCheck();
#if (USE_EXT_COMM())
		modextcomCheck();
#endif
		bootmasterMain();
#ifdef MAIN_STANDALONE
		standaloneMain();
#endif
		DISABLE_IRQ( );
		/* if an interrupt has occurred after DISABLE_IRQ, it is kept pending
		 * and cortex will not enter low power anyway  */

		if (!stLORA_USER_CTX.bLoraMACProcRqst)
		{
#ifndef LOW_POWER_DISABLE
			LPM_EnterLowPower( );
#endif
		}

		ENABLE_IRQ();
 	}
}
