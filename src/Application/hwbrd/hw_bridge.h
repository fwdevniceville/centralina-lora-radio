/*
 * hw_uart_brd.h
 *
 *  Created on: 11 mar 2018
 *      Author: daniele_parise
 */

#ifndef SRC_APPLICATION_HWBRD_HW_BRIDGE_H_
#define SRC_APPLICATION_HWBRD_HW_BRIDGE_H_

#include "hal_inc.h"
void hw_bridge_gpio_clock_enable(GPIO_TypeDef * uiPORT,unsigned uiPIN);
void hw_bridge_gpio_clock_disable(GPIO_TypeDef * uiPORT,unsigned uiPIN);
void hw_bridge_gpio_clock_set(GPIO_TypeDef * uiPORT,unsigned uiPIN,bool bSet);









#endif /* SRC_APPLICATION_HWBRD_HW_BRIDGE_H_ */
