/*
 * hw_uart_brd.c
 *
 *  Created on: 11 mar 2018
 *      Author: daniele_parise
 */

#include <stdbool.h>
#include <stdint.h>
#include "hw.h"
#include "vcom.h"
#include "hw_bridge.h"
#include "../intcom/intcom.h"
#if (USE_EXT_COMM())
#include "../extcom/extcom.h"
#endif



uint16_t	uiPORTA_CKL_IMAGE;

void hw_bridge_gpio_clock_set(GPIO_TypeDef * uiPORT,unsigned uiPIN,bool bSet)
{
	uint16_t*	puiPORT_CKL_IMAGE;
	uint16_t	uiNULL_PORT;
	uint16_t	uiMASK;
	puiPORT_CKL_IMAGE = &uiNULL_PORT;
	switch ((uintptr_t)uiPORT)
	{
//	case (uintptr_t)GPIOA:
	case GPIOA_BASE:
		{
			puiPORT_CKL_IMAGE = &uiPORTA_CKL_IMAGE;
		}
		break;
	}

	uiMASK = 1<<uiPIN;

	if (bSet)
	{
		*puiPORT_CKL_IMAGE |= uiMASK;
	}
	else
	{
		*puiPORT_CKL_IMAGE &= ~uiMASK;
	}
	switch ((uintptr_t)uiPORT)
	{
	case (uintptr_t)GPIOA_BASE:
		{
			if (uiPORTA_CKL_IMAGE)
			{
				__GPIOA_CLK_ENABLE();
			}
			else
			{
				__GPIOA_CLK_DISABLE();
			}
		}
		break;
	}
}

void hw_bridge_gpio_clock_enable(GPIO_TypeDef * uiPORT,unsigned uiPIN)
{
	hw_bridge_gpio_clock_set(uiPORT,uiPIN,true);
}




void hw_bridge_gpio_clock_disable(GPIO_TypeDef * uiPORT,unsigned uiPIN)
{
	hw_bridge_gpio_clock_set(uiPORT,uiPIN,false);
}






/**
  * @brief UART MSP Initialization
  *        This function configures the hardware resources used in this example:
  *           - Peripheral's clock enable
  *           - Peripheral's GPIO Configuration
  *           - NVIC configuration for UART interrupt request enable
  * @param huart: UART handle pointer
  * @retval None
  */
void HAL_UART_MspInit(UART_HandleTypeDef *huart)
{
	if (huart == &UartIntcomHandle)
	{
		__USART1_CLK_ENABLE();
		intcom_IoInit( );
	}
#if (USE_EXT_COMM())
	else
	if (huart == &UartExtcomHandle)
	{
		__USART2_CLK_ENABLE();
		extcom_IoInit( );
	}
#endif
/*
	else
	if (huart == &UartHandle)
	{
		__USART2_CLK_ENABLE();
		vcom_IoInit( );
	}
	*/
}

void HAL_UART_MspDeInit(UART_HandleTypeDef *huart)
{
	if (huart == &UartIntcomHandle)
	{
		__USART1_CLK_ENABLE();
		intcom_IoDeInit( );
	}
#if (USE_EXT_COMM())
	else
	if (huart == &UartExtcomHandle)
	{
		__USART2_CLK_ENABLE();
		extcom_IoDeInit( );
	}
#endif
/*
	else
	if (huart == &UartHandle)
	{
		__USART2_CLK_ENABLE();
		vcom_IoDeInit( );
	}
*/
}



void USART1_IRQHandler( void )
{
   intcom_IRQHandler( );
}


void USART2_IRQHandler( void )
{
#if (USE_EXT_COMM())
	extcom_IRQHandler( );
#endif
}





